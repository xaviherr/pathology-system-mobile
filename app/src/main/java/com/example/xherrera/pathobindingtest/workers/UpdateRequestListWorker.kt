package com.example.xherrera.pathobindingtest.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.xherrera.pathobindingtest.database.PathoDatabase
import com.example.xherrera.pathobindingtest.extDb.ApiService

class UpdateRequestListWorker(context: Context, params: WorkerParameters): Worker(context, params) {

    private val dBase by lazy {
        PathoDatabase.getInstance(context)
    }
    private val apiServe by lazy {
        ApiService.create()
    }

    override fun doWork(): Result {
        makeStatusNotification("Checking for new Requests", applicationContext)

        val remoteRqList = apiServe.getRequests().execute().body()
        val localRqList = dBase.requestDao().getRequestList()

        if (remoteRqList!! != localRqList){
            makeStatusNotification("Updating Request List", applicationContext)
            dBase.requestDao().insertAll(remoteRqList)
            return Result.success()
        }

        return Result.retry()
    }
}