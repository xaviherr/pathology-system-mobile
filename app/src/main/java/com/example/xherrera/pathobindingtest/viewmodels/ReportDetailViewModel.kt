package com.example.xherrera.pathobindingtest.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.xherrera.pathobindingtest.database.entities.Lecture
import com.example.xherrera.pathobindingtest.extDb.ApiService

class ReportDetailViewModel(
    requestId: Int,
    essayId: Int,
    agentId: Int
): ViewModel() {

    val resultList: MutableLiveData<ArrayList<Lecture>> = MutableLiveData()

    private val apiServe by lazy{
        ApiService.create()
    }

    init {
        Log.i("ReportDetailViewModel", "ReporDetailViewModel has been created")
        resultList.value = apiServe.getLecturesReport(requestId, essayId, agentId).execute().body()!!
    }
}