package com.example.xherrera.pathobindingtest.lab

import android.util.Log
import com.example.xherrera.pathobindingtest.database.entities.*
import kotlin.math.ceil
import kotlin.math.round
import kotlin.random.Random

interface LabDistribution {

    fun genEssayDistributor(essayBrief: EssayBrief): EssayDistributor{

        val totalPositions = arrayListOf<Int>()
        val controlPositions = arrayListOf<Int>()
        val tenPercentMasterPositions = arrayListOf<Int>()
        val tenPercentPositions = arrayListOf<Int>()
        val problemPositions = arrayListOf<Int>()
        val blindPositions = arrayListOf<Int>()
        val dBlindPositions = arrayListOf<Int>()
        val emptyPositions = arrayListOf<Int>()
        val controlBySupport = essayBrief.controlBySupport
        val controlLastSupport = essayBrief.controlLastSupport

        /**
         * CREATING CONTROL, PROBLEM AND EMPTY POSITIONS LIST
         */
        for (i in 1..essayBrief.supportByPathogen){
            var problemPos: Int
            if (i == essayBrief.supportByPathogen){
                for (j in (essayBrief.rduByPathogen-(controlLastSupport-1))
                        ..essayBrief.rduByPathogen){
                    controlPositions.add(j)
                }
                if (essayBrief.emptyPositions != 0){
                    for (k in (essayBrief.rduByPathogen-essayBrief.emptyPositions-(controlLastSupport-1))
                            ..(essayBrief.rduByPathogen-(controlLastSupport))){
                        emptyPositions.add(k)
                    }
                }
                if (essayBrief.problemByPathogen > 0){
                    problemPos = Random.nextInt((essayBrief.rduByPathogen - (essayBrief.rduByPathogen % essayBrief.slotBySupport)),
                        ((essayBrief.rduByPathogen - (controlLastSupport-1))-essayBrief.emptyPositions))
                    Log.d("Problem at last support", "$problemPos")
                    problemPositions.add(problemPos)
                }
            }
            else{
                for (j in (essayBrief.slotBySupport-(controlBySupport-1))..essayBrief.slotBySupport){
                    controlPositions.add(j+(essayBrief.slotBySupport*(i-1)))
                }
                if (essayBrief.problemByPathogen > 0){
                    problemPos = Random.nextInt(1, (essayBrief.slotBySupport - controlBySupport))
                    Log.d("Problem at $i support", "$problemPos")
                    problemPositions.add(problemPos+(essayBrief.slotBySupport*(i-1)))
                }
            }
        }
        Log.d("Control Positions", "$controlPositions")
        Log.d("Problem Positions", "$problemPositions")
        Log.d("Empty Positions", "$emptyPositions")

        /**
         * CREATING TEN PERCENT POSITIONS LIST
         */
        for (i in 1..essayBrief.tenPercentSlots){
            var tenRepPos = Random.nextInt(1, essayBrief.rduByPathogen)
            while ((tenRepPos%2==0)||
                (tenRepPos in controlPositions)||
                ((tenRepPos+1) in controlPositions)||
                (tenRepPos in emptyPositions)||
                (tenRepPos+1 in emptyPositions)||
                (tenRepPos in problemPositions)||
                (tenRepPos+1 in problemPositions)||
                (tenRepPos in tenPercentMasterPositions)){
                tenRepPos = Random.nextInt(1, essayBrief.rduByPathogen)
            }
            val tenPos = tenRepPos + 1
            tenPercentMasterPositions.add(tenRepPos)
            tenPercentPositions.add(tenPos)
        }
        Log.d("tenPercentMasterPos", "$tenPercentMasterPositions")
        Log.d("tenPercentPositions", "$tenPercentPositions")

        /**
         * CREATING BLIND AND DOUBLE BLIND POSITIONS LIST
         */
        //TODO Include segmentation at Support level
        totalPositions.addAll(1..essayBrief.rduByPathogen)
        val shuffledAvailablePositions = (totalPositions
                - controlPositions - problemPositions - emptyPositions
                - tenPercentMasterPositions - tenPercentPositions).shuffled()
        for (i in 1..essayBrief.blindQty){
            blindPositions.add(shuffledAvailablePositions[i-1])
        }
        Log.d("Blind Positions", "$blindPositions")

        for (i in 1..essayBrief.dBlindQty){
            dBlindPositions.add(shuffledAvailablePositions[essayBrief.blindQty+i-1])
        }
        Log.d("Double Blind Positions", "$dBlindPositions")

        return EssayDistributor(
            controlPositions,
            tenPercentPositions,
            problemPositions,
            blindPositions,
            dBlindPositions,
            emptyPositions
        )
    }

    fun genSlots(
        essayBrief: EssayBrief,
        essayDistributor: EssayDistributor,
        agentList: List<AgentByProcess>,
        sampleList: List<Sample>,
        activityList: List<Activity>
    ): ArrayList<Slot>{
        val slotList = arrayListOf<Slot>()

        Log.d("Activities for Slots", "$activityList")
        activityList.forEach{
            for (p in 1..essayBrief.pathogenQty){

                var avoidCells = 0

                for (s in 1..essayBrief.rduByPathogen){

                    val type: String
                    val sampleId: Int
                    val cellPos = if((s % essayBrief.slotBySupport)!=0){
                        (s % essayBrief.slotBySupport)
                    } else { 96 } //01-10-2020

                    when (s) {
                        in essayDistributor.controlPositionList -> {
                            type = "control"
                            sampleId = 999
                            avoidCells++
                        }
                        in essayDistributor.problemPositionList -> {
                            type = "problem"
                            sampleId = 999
                            avoidCells++
                        }
                        in essayDistributor.blindPositionList -> {
                            type = "blind"
                            sampleId = 999
                            avoidCells++
                        }
                        in essayDistributor.dBlindPositionList -> {
                            type = "dBlind"
                            sampleId = 999
                            avoidCells++
                        }
                        in essayDistributor.emptyPositionList -> {
                            type = "empty"
                            sampleId = 999
                            avoidCells++
                        }
                        in essayDistributor.tenPercentPositionList -> {
                            type = "10%"
                            sampleId = slotList.last().sampleId
                            avoidCells++
                        }
                        else -> {
                            type = "sample"
                            sampleId = sampleList[((s-avoidCells)-1)].sampleId
                        }
                    }

                    val slot = Slot(
                        labProcessId = (s-avoidCells),
                        agentId = agentList[p - 1].agentId,
                        sampleId = sampleId,
                        supportId = ceil(s/essayBrief.slotBySupport.toDouble()).toInt()+(essayBrief.supportByPathogen*(p-1)),
                        cellPosition = cellPos,
                        type = type,
                        essayId = essayBrief.essayId,
                        requestId = essayBrief.requestId,
                        cropId = essayBrief.cropId,
                        workflowId = essayBrief.workflowId,
                        activityId = it.activityId,
                        numOrderId = essayBrief.numOrderId
                    )
                    slotList.add(slot)
                    println(slot)
                    Log.d("Current avoid", "$avoidCells")
                }
            }
        }
        Log.d("Slot List size", "${slotList.size}")
        return slotList
    }

    fun genPcrSlots(
        sampleList: List<Sample>,
        agentList: List<AgentByProcess>,
        gelBrief: PcrGelBrief
    ): ArrayList<Slot>{
        val slotList = arrayListOf<Slot>()
        var labProcessId = 0

        // First ladder
        if (gelBrief.ladderQty > 0){
            //labProcessId += 1
            agentList.forEach {
                val slot = Slot(
                    labProcessId = 0,
                    activityId = gelBrief.activityId,
                    supportId = gelBrief.supportId,
                    requestId = gelBrief.requestId,
                    essayId = gelBrief.essayId,
                    numOrderId = gelBrief.numOrderId,
                    cropId = gelBrief.cropId,
                    workflowId = gelBrief.workflowId,
                    sampleId = 999,
                    agentId = it.agentId,
                    agentName = it.agentName,
                    cellPosition = labProcessId,
                    type = "ladder",
                    result = "ladder"
                )
                slotList.add(slot)
            }
        }

        // Sample Slots
        sampleList.forEach {sample ->
            labProcessId += 1
            agentList.forEach {
                val slot = Slot(
                    labProcessId = sample.numOrder ?: labProcessId,
                    activityId = gelBrief.activityId,
                    supportId = gelBrief.supportId,
                    requestId = gelBrief.requestId,
                    essayId = gelBrief.essayId,
                    numOrderId = gelBrief.numOrderId,
                    cropId = gelBrief.cropId,
                    workflowId = gelBrief.workflowId,
                    sampleId = sample.sampleId,
                    agentId = it.agentId,
                    agentName = it.agentName,
                    cellPosition = labProcessId,
                    type = "sample"
                )
                slotList.add(slot)
            }
        }

        //Controls
        for (i in 1..gelBrief.controlQty){
            labProcessId += 1
            agentList.forEach {
                val slot = Slot(
                    labProcessId = 0,
                    activityId = gelBrief.activityId,
                    supportId = gelBrief.supportId,
                    requestId = gelBrief.requestId,
                    essayId = gelBrief.essayId,
                    numOrderId = gelBrief.numOrderId,
                    cropId = gelBrief.cropId,
                    workflowId = gelBrief.workflowId,
                    sampleId = 999,
                    agentId = it.agentId,
                    agentName = it.agentName,
                    cellPosition = labProcessId,
                    type = "control"
                )
                slotList.add(slot)
            }
        }
        //Second ladder
        if (gelBrief.ladderQty > 1){
            labProcessId += 1
            agentList.forEach {
                val slot = Slot(
                    labProcessId = 0,
                    activityId = gelBrief.activityId,
                    supportId = gelBrief.supportId,
                    requestId = gelBrief.requestId,
                    essayId = gelBrief.essayId,
                    numOrderId = gelBrief.numOrderId,
                    cropId = gelBrief.cropId,
                    workflowId = gelBrief.workflowId,
                    sampleId = 999,
                    agentId = it.agentId,
                    agentName = it.agentName,
                    cellPosition = labProcessId,
                    type = "ladder",
                    result = "ladder"
                )
                slotList.add(slot)
            }
        }
        println(slotList.first())
        println(slotList.last())
        println("Slots created: ${slotList.count()}")
        return slotList
    }

    fun genPcrSupports(
        essayBrief: EssayBrief,
        activityList: List<Activity>
    ): ArrayList<Support>{
        val supportList = arrayListOf<Support>()

        Log.d("Activities for Supports", "$activityList")
        activityList.forEach {
            for (s in 1..essayBrief.supportByPathogen){
                val support = Support(
                    supportId = s,
                    totalSlots = 0,
                    requestId = essayBrief.requestId,
                    agentName = "PCR", //TODO Update when the specific gel is configured
                    essayId = essayBrief.essayId,
                    numOrderId = essayBrief.numOrderId,
                    cropId = essayBrief.cropId,
                    workFlowId = essayBrief.workflowId,
                    activityId = it.activityId,
                    ctrlQty = 0
                )
                supportList.add(support)
            }
        }
        Log.d("Support List size", "${supportList.size}")
        return supportList
    }

    fun genSupports(
        essayBrief: EssayBrief,
        agentList: List<AgentByProcess>,
        activityList: List<Activity>
    ): ArrayList<Support>{
        val supportList = arrayListOf<Support>()

        Log.d("Activities for Supports", "$activityList")
        activityList.forEach{
            for (p in 1..essayBrief.pathogenQty){ //agentList.foreach()
                for (s in 1..essayBrief.supportByPathogen){
                    val support = Support(
                        supportId = s + (essayBrief.supportByPathogen*(p-1)),
                        totalSlots = essayBrief.slotBySupport,
                        requestId = essayBrief.requestId,
                        agentName = agentList[p-1].agentName,
                        essayId = essayBrief.essayId,
                        numOrderId = essayBrief.numOrderId,
                        cropId = essayBrief.cropId,
                        workFlowId = essayBrief.workflowId,
                        activityId = it.activityId
                    )
                    supportList.add(support)
                }
            }
        }
        Log.d("Support List size", "${supportList.size}")
        return supportList
    }

    fun updateEssayBrief(
        essayBrief: EssayBrief,
        newControlsBySupport: Int?,
        controlLastSupport: Int,
        emptySlots: Int=0
    ): EssayBrief{

        var controlsBySupport = 6
        var noControlSlotsBySupport = essayBrief.slotBySupport - controlsBySupport
        var problemsBySupport = 0
        var tenPercentQty = 0
        val noControlByPathogenQty: Int
        val supportByPathogenQty: Int
        val rduByPathogenQty: Int
        val controlsByPathogen: Int
        val problemsByPathogen: Int

        if (essayBrief.withTenPercent){
            tenPercentQty = round(essayBrief.sampleQty * 0.1).toInt()
        }

        if (essayBrief.essayType == 4){
            controlsBySupport = 5
            problemsBySupport = 1
        }

        if (newControlsBySupport != null) {
            controlsBySupport = newControlsBySupport
            noControlSlotsBySupport = essayBrief.slotBySupport - controlsBySupport
        }

        noControlByPathogenQty = essayBrief.sampleQty +
                essayBrief.blindQty +
                essayBrief.dBlindQty +
                tenPercentQty

        supportByPathogenQty = ceil(
            noControlByPathogenQty.toDouble() / noControlSlotsBySupport.toDouble()
        ).toInt()

        controlsByPathogen = ((supportByPathogenQty-1) * controlsBySupport) + controlLastSupport
        problemsByPathogen = supportByPathogenQty * problemsBySupport

        rduByPathogenQty = noControlByPathogenQty + controlsByPathogen + problemsByPathogen + emptySlots

        essayBrief.emptyPositions = emptySlots
        essayBrief.controlBySupport = controlsBySupport
        essayBrief.controlLastSupport = controlLastSupport
        essayBrief.supportByPathogen = supportByPathogenQty
        essayBrief.controlByPathogen = controlsByPathogen
        essayBrief.problemByPathogen = problemsByPathogen
        essayBrief.rduByPathogen = rduByPathogenQty
        essayBrief.tenPercentSlots = tenPercentQty

        return essayBrief
    }
}