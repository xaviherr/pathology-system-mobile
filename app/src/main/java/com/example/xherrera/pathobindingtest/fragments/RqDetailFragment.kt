package com.example.xherrera.pathobindingtest.fragments

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.RqDetailAdapter
import com.example.xherrera.pathobindingtest.database.entities.AgentByProcess
import com.example.xherrera.pathobindingtest.database.entities.ReqProcess
import com.example.xherrera.pathobindingtest.database.entities.Sample
import com.example.xherrera.pathobindingtest.databinding.FragmentRqDetailBinding
import com.example.xherrera.pathobindingtest.viewmodels.EssayViewModel
import com.example.xherrera.pathobindingtest.viewmodels.GreenHouseViewModel
import com.example.xherrera.pathobindingtest.viewmodels.RequestViewModel
import kotlinx.android.synthetic.main.fragment_rq_detail.view.*
import com.example.xherrera.pathobindingtest.App.Companion.isNetworkAvailable
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main


@ExperimentalCoroutinesApi
class RqDetailFragment : Fragment(), RqDetailAdapter.Listener {

    private lateinit var agentList: List<AgentByProcess>
    private lateinit var sampleList: List<Sample>
    private lateinit var rqCode: String
    private var requestId: Int? = 0
    private var numOrderId: Int? = 0
    private lateinit var essayViewModel: EssayViewModel
    private var agentQty: Int = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.i("RqDetailFragment", "RqDetailFragment attached")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val args = RqDetailFragmentArgs.fromBundle(this.arguments!!)
        requestId = args.rqId
        rqCode = args.rqCode
        val sampleQty = args.sampleQty
        agentQty = args.pathoQty
        numOrderId = args.numOrderId

        val binding = DataBindingUtil.inflate<FragmentRqDetailBinding>(
            inflater,
            R.layout.fragment_rq_detail,
            container,
            false
        )

        val requestViewModel = ViewModelProviders.of(this).get(RequestViewModel::class.java)
        essayViewModel = ViewModelProviders.of(this).get(EssayViewModel::class.java)

        requestViewModel.purgeLocalRequestData(requestId!!, sampleQty, agentQty)

        if (!isNetworkAvailable()) {
            androidx.appcompat.app.AlertDialog
                .Builder(activity!!)
                .setTitle("Error retrieving data")
                .setMessage("Network connection is not available.\nPlease try again later.")
                .setPositiveButton("OK", null)
                .create().show()
        }

        requestViewModel.getRqDetails(requestId!!, numOrderId!!)
        requestViewModel.getAgentsByProcess(requestId!!)
        requestViewModel.getSamples(requestId!!, numOrderId!!)
        requestViewModel.getActivities()

        requestViewModel.sampleListLiveData.observe(this, Observer {
            sampleList = it
        })
        requestViewModel.agentByProcessList.observe(this, Observer {
            agentList = it
        })

        var agentString = ""
        requestViewModel.agentByProcessList.observe(this, Observer {
            val pathogenList = it.filter { agent -> agent.agentGroupId == 87 }
            for (i in pathogenList) {
                agentString += if (i == it.last()) i.agentName else i.agentName + ", "
            }
        })

        /**
         * BINDING TEXTVIEWS VALUES
         */
        binding.tvRqCode.text = args.rqCode
        binding.tvRqDetails.text = args.rqDetails
        binding.tvRqCropName.text = args.rqCropName
        binding.tvReqDate.text = args.reqDate

        requestViewModel.agentByProcessList.observe(this, Observer {
            if (it.filter { agent -> agent.agentGroupId == 87 }.count() < 4) {
                binding.root.tvAgents.text = agentString
            } else {
                binding.tvAgents.text = "Touch to see the Agents List"
                binding.tvAgents.setTextColor(Color.parseColor("#388E3C"))
                binding.tvAgents.isClickable = true
                binding.tvAgents.setOnClickListener {
                    AlertDialog.Builder(activity)
                        .setTitle("Agents List")
                        .setMessage(agentString)
                        .setPositiveButton("OK", null)
                        .create().show()
                }
            }
        })

        /**
         * MANAGING THE REQUEST PROCESSES RECYCLER VIEW
         */
        val recyclerView = binding.root.rvReqProcesses
        val manager = LinearLayoutManager(activity)
        recyclerView.layoutManager = manager
        recyclerView.itemAnimator = DefaultItemAnimator()
        requestViewModel.requestProcesses.observe(this, Observer {
            recyclerView.adapter = RqDetailAdapter(it, this)
            /*it.forEach { process ->
                essayViewModel.getServerPack(process.requestId, process.essayId)
            }*/
        })

        println(requestViewModel.agentByProcessList.value)

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu_reset, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.resetRequest) {
            androidx.appcompat.app.AlertDialog
                .Builder(activity!!)
                .setTitle("Reset Request Warning")
                .setMessage("This action will delete your local data permanently.\nDo you want to proceed?")
                .setPositiveButton("RESET DATA") { _, _ ->
                    val requestViewModel =
                        ViewModelProviders.of(this).get(RequestViewModel::class.java)
                    //DELETE DATA
                    requestViewModel.deleteRequestData(this.requestId!!)
                    requestViewModel.getRqDetails(requestId!!, numOrderId!!)
                    requestViewModel.getAgentsByProcess(requestId!!)
                    requestViewModel.getSamples(requestId!!, numOrderId!!)
                    requestViewModel.getActivities()
                    //NAVIGATE TO REQUEST LIST

                }
                .setNegativeButton("CANCEL", null)
                .create().show()
            return true
        }

        return NavigationUI.onNavDestinationSelected(
            item,
            view!!.findNavController()
        )
                || super.onOptionsItemSelected(item)
    }

    override fun onItemClick(reqProcess: ReqProcess) {

        println(essayViewModel.essayAgents.value)
        println(reqProcess.essayId)
        println(reqProcess)
        val navController = findNavController()
        essayViewModel.getServerPack(reqProcess.requestId, reqProcess.essayId)

        /**
         * CHECK THIS CONDITIONAL NAV BEHAVIOUR
         */

        if (reqProcess.essayId == 2 || reqProcess.essayId == 3) {
            val ghViewModel = ViewModelProviders.of(this).get(GreenHouseViewModel::class.java)
            ghViewModel.getBioPack(reqProcess.requestId, reqProcess.essayId, numOrderId!!)

            ghViewModel.ghPlants.observe(viewLifecycleOwner, Observer {
                when {
                    it.isNullOrEmpty() -> navController.navigate(
                        RqDetailFragmentDirections.actionRqDetailFragmentToBioEssaySetupFragment(
                            rqCode,
                            reqProcess.requestId,
                            reqProcess.essayId,
                            reqProcess.numOrderId
                        )
                    )
                    else -> navController.navigate(
                        RqDetailFragmentDirections.actionRqDetailFragmentToBioEssayDashFragment(
                            reqProcess.requestId,
                            rqCode,
                            reqProcess.essayId,
                            reqProcess.numOrderId
                        )
                    )
                }
            })
        } else if (reqProcess.essayId == 1 || reqProcess.essayId == 4
            || reqProcess.essayId == 5 || reqProcess.essayId == 12) {

            essayViewModel.getEssayPack(reqProcess.requestId, reqProcess.essayId, numOrderId!!)

            essayViewModel.essaySlots.observe(this.viewLifecycleOwner, Observer {
                when{
                    it.isNullOrEmpty() -> navController.navigate(RqDetailFragmentDirections
                        .actionRqDetailFragmentToSupportsSetupFragment(
                            agentList.count(),
                            reqProcess.requestId,
                            rqCode,
                            reqProcess.essayId,
                            reqProcess.numOrderId
                        ))

                    else -> navController.navigate(RqDetailFragmentDirections
                        .actionRqDetailFragmentToSupDashboardFragment(
                            reqProcess.requestId,
                            false,
                            reqProcess.essayId,
                            reqProcess.numOrderId,
                            rqCode
                        ))
                }
            })
        } else {

            essayViewModel.getEssayPack(reqProcess.requestId, reqProcess.essayId, numOrderId!!)

            essayViewModel.essaySlots.observe(this.viewLifecycleOwner, Observer {
                when{
                    it.isNullOrEmpty() -> navController.navigate(RqDetailFragmentDirections
                        .actionRqDetailFragmentToPcrSuppSetupFragment(
                            agentList.count(),
                            reqProcess.requestId,
                            rqCode,
                            reqProcess.essayId,
                            reqProcess.numOrderId
                        ))
                    else -> navController.navigate(RqDetailFragmentDirections
                        .actionRqDetailFragmentToPcrGelListFragment(
                            agentQty,
                            reqProcess.requestId,
                            rqCode,
                            reqProcess.essayId,
                            reqProcess.numOrderId
                        ))
                }
            })
        }
    }
}
