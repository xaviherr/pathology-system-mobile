package com.example.xherrera.pathobindingtest.database.entities

data class BioPack(
    val bioEssSamples: List<Sample>,
    val bioEssPlants: List<PlantBySample>,
    val bioEssPlantSlots: List<PlantSlot>
)