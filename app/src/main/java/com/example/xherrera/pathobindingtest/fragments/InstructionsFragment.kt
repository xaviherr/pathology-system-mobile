package com.example.xherrera.pathobindingtest.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.databinding.FragmentInstructionsBinding

class InstructionsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentInstructionsBinding>(inflater,
            R.layout.fragment_instructions, container, false)

        return binding.root
    }


}
