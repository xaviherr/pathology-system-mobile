package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["supportId", "requestId", "essayId", "activityId"])
data class Support(
    @SerializedName("supportId") @Expose @ColumnInfo(name ="supportId") var supportId : Int,
    @SerializedName("columns") @Expose @ColumnInfo(name ="columns")  val columns: Int = 12,
    @SerializedName("rows") @Expose @ColumnInfo(name ="rows") val rows: Int = 8,
    @SerializedName("totalSlots") @Expose @ColumnInfo(name ="totalSlots") val totalSlots: Int = rows * columns,
    @SerializedName("usedSlots") @Expose @ColumnInfo(name ="usedSlots") var usedSlots: Int = 0,
    @SerializedName("availableSlots") @Expose @ColumnInfo(name ="availableSlots") var availableSlots: Int = totalSlots - usedSlots,
    @SerializedName("ctrlQty") @Expose @ColumnInfo(name ="ctrlQty") var ctrlQty: Int = 6,
    @SerializedName("workFlowId") @Expose @ColumnInfo(name = "workFlowId") val workFlowId: Int,
    @SerializedName("cropId") @Expose @ColumnInfo(name = "cropId") val cropId: Int,
    @SerializedName("numOrderId") @Expose @ColumnInfo(name = "numOrderId") val numOrderId: Int,
    @SerializedName("activityId") @Expose @ColumnInfo(name = "activityId") val activityId: Int,
    @SerializedName("supportTypeId") @Expose @ColumnInfo(name ="supportTypeId") var supportTypeId: Int = 81,
    @SerializedName("requestId") @Expose @ColumnInfo(name ="requestId") var requestId: Int,
    @SerializedName("essayId") @Expose @ColumnInfo(name ="essayId") var essayId: Int,
    @SerializedName("agentName") @Expose @ColumnInfo(name ="agentName") val agentName: String?
)