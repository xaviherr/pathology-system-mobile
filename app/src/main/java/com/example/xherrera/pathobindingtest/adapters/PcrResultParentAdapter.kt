package com.example.xherrera.pathobindingtest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Slot
import kotlinx.android.synthetic.main.item_pcr_parent_list.view.*


class PcrResultParentAdapter(
    private val sampleList: ArrayList<Int>,
    slotList: ArrayList<Slot>
    ): RecyclerView.Adapter<PcrResultParentAdapter.ViewHolder>(), PcrResultChildAdapter.Listener {

    private var bySample = slotList.groupBy { it.cellPosition }
    private lateinit var childRecycler: RecyclerView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pcr_parent_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val slotBySample = bySample.values.toMutableList()
        childRecycler = holder.itemView.rv_samp_slots
        val adapter = PcrResultChildAdapter(slotBySample[position] as ArrayList<Slot>, this)
        holder.bind(sampleList[position])

        childRecycler.layoutManager = LinearLayoutManager(childRecycler.context, LinearLayoutManager.HORIZONTAL, false)
        childRecycler.setItemViewCacheSize(100)
        childRecycler.itemAnimator = DefaultItemAnimator()
        childRecycler.adapter = adapter
    }

    override fun getItemCount(): Int {
        return sampleList.count()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(sample: Int){
            itemView.tv_samp_id.text = sample.toString()
            itemView.rv_samp_slots.itemAnimator = DefaultItemAnimator()
        }
    }

    override fun onItemClick(slot: Slot) {
        println("Slot ${slot.labProcessId} clicked | ${slot.result}")
        when (slot.result) {
            "negative" -> {
                slot.result="positive"
                childRecycler.adapter?.notifyDataSetChanged()
                this.notifyDataSetChanged()
            }
            "positive" -> {
                slot.result="doubt"
                childRecycler.adapter?.notifyDataSetChanged()
                this.notifyDataSetChanged()
            }
            "doubt" -> {
                slot.result="buffer"
                childRecycler.adapter?.notifyDataSetChanged()
                this.notifyDataSetChanged()
            }
            "buffer" -> {
                slot.result="ladder"
                childRecycler.adapter?.notifyDataSetChanged()
                this.notifyDataSetChanged()
            }
            else -> {
                slot.result = "negative"
                childRecycler.adapter?.notifyDataSetChanged()
                this.notifyDataSetChanged()
            }
        }
        childRecycler.adapter?.notifyDataSetChanged()
        this.notifyDataSetChanged()
    }
}