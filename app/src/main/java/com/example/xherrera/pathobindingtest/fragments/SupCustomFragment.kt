package com.example.xherrera.pathobindingtest.fragments


import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.ChildCustomAdapter
import com.example.xherrera.pathobindingtest.adapters.SuppCustomAdapter
import com.example.xherrera.pathobindingtest.database.entities.*
import com.example.xherrera.pathobindingtest.databinding.FragmentSupCustomBinding
import com.example.xherrera.pathobindingtest.viewmodels.EssayViewModel
import kotlinx.android.synthetic.main.fragment_sup_custom.*
import kotlinx.android.synthetic.main.fragment_sup_custom.view.*

class SupCustomFragment : Fragment(), SuppCustomAdapter.Listener, ChildCustomAdapter.Listener {

    private var usedSlots = 0
    private var option: Int? = null
    private var custSlots: ArrayList<Slot>? = null
    private var custSupps: List<Support>? = null
    private var pathos: List<AgentByProcess>? = null
    private var samples: MutableList<Sample>? = null
    private val custBlindList: ArrayList<Slot> = ArrayList()
    private val custCtrlList: ArrayList<Slot> = ArrayList()
    private val custEmptyList: ArrayList<Slot> = ArrayList()
    private var suppList: ArrayList<Support>? = null
    private var slotList: ArrayList<Slot>? = null

    private var recyclerView: RecyclerView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        val binding = DataBindingUtil.inflate<FragmentSupCustomBinding>(inflater,
            R.layout.fragment_sup_custom, container, false)

        /**
         * ESSAYVIEWMODEL AND VALUES INIT
         */
        val essayViewModel= ViewModelProviders.of(activity!!).get(EssayViewModel::class.java)
        custSlots = essayViewModel.essaySlots.value!! as ArrayList<Slot>
        custSupps = essayViewModel.essaySupports.value!!
        essayViewModel.essaySupports.postValue(custSupps as List<Support>)
        pathos = essayViewModel.essayAgents.value!!
        samples = essayViewModel.essaySamples.value!!

        suppList = essayViewModel.essaySupports.value!! as ArrayList<Support>
        slotList = essayViewModel.essaySlots.value!! as ArrayList<Slot>

        /**
         * RECYCLERVIEW INIT
         */
        recyclerView = binding.root.rvSuppHor
        val manager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        recyclerView!!.layoutManager = manager
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = SuppCustomAdapter(suppList, slotList!!, this, this)

        /**
         * CUSTOMIZATION MENU BUTTONS
         */
        binding.imgSetStart.setOnClickListener {
            imgSetStart.setImageResource(R.drawable.ic_setstart_active)
            imgSetEmpty.setImageResource(R.drawable.ic_setempty_inactive)
            imgSetBlind.setImageResource(R.drawable.ic_setblind_inactive)
            imgSetControl.setImageResource(R.drawable.ic_setcontrol_inactive)
            option = 1
        }
        binding.root.imgSetBlind.setOnClickListener {
            imgSetBlind.setImageResource(R.drawable.ic_setblind_active)
            imgSetEmpty.setImageResource(R.drawable.ic_setempty_inactive)
            imgSetStart.setImageResource(R.drawable.ic_setstart_inactive)
            imgSetControl.setImageResource(R.drawable.ic_setcontrol_inactive)
            option = 2
        }
        binding.root.imgSetControl.setOnClickListener {
            imgSetControl.setImageResource(R.drawable.ic_setcontrol_active)
            imgSetEmpty.setImageResource(R.drawable.ic_setempty_inactive)
            imgSetBlind.setImageResource(R.drawable.ic_setblind_inactive)
            imgSetStart.setImageResource(R.drawable.ic_setstart_inactive)
            option = 3
        }
        binding.imgSetEmpty.setOnClickListener {
            imgSetEmpty.setImageResource(R.drawable.ic_setempty_active)
            imgSetStart.setImageResource(R.drawable.ic_setstart_inactive)
            imgSetBlind.setImageResource(R.drawable.ic_setblind_inactive)
            imgSetControl.setImageResource(R.drawable.ic_setcontrol_inactive)
            option = 4
        }

        /**
         * SAVE & SEND BUTTON
         */
        binding.btnDone.setOnClickListener {
            Toast.makeText(this.activity, "Changes saved", Toast.LENGTH_LONG).show()
            essayViewModel.essaySupports.value = suppList
            essayViewModel.essaySlots.value = slotList
        }

        /**
         * HEADER MENU
         */
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }

    /**
     * FATHER RECYCLERVIEW ONCLICKLISTENER TO NOTIFY ITEM CHANGES (CHECK IT OUT LATER)
     */
    override fun onItemClick(support: Support, position: Int, slotList: ArrayList<Slot>) {
        Toast.makeText(this.activity, "Option selected: $option", Toast.LENGTH_SHORT).show()
        recyclerView!!.adapter!!.notifyItemChanged(position)
        recyclerView!!.adapter!!.notifyDataSetChanged()
    }

    /**
     * CHILD RECYCLERVIEW ONCLICKLISTENER FOR CELLS/READINGDATAUNITS MODIFICATIONS
     */
    override fun onItemClick(slot: Slot, position: Int) {

        when(option){
            1 -> {
                slot.type = "first"
                usedSlots = slot.cellPosition - 1
            }
            2 -> {
                displaceSlots(slot)
                slot.type = "blind"
                slot.sampleId = 999
                custBlindList.add(slot)
            }
            3 -> {
                displaceSlots(slot)
                slot.type = "control"
                slot.sampleId = 999
                custCtrlList.add(slot)
            }
            4 -> {
                displaceSlots(slot)
                slot.type = "empty"
                slot.sampleId = 999
                custEmptyList.add(slot)
            }
            else -> Toast.makeText(this.activity, "Choose an option", Toast.LENGTH_SHORT).show()
        }
        recyclerView!!.adapter!!.notifyItemChanged(position)
        recyclerView!!.adapter!!.notifyDataSetChanged()
    }

    /**
     * FUNCTION TO DISPLACE THE SAMPLE SLOT CHANGED
     * CREATES A NEW SUPPORT IF NECESSARY
     */
    private fun displaceSlots(slot: Slot){
        val lastCell = slotList!!.last().cellPosition
        val newCell = if (lastCell==96) {1} else{lastCell+1}
        if (lastCell == 96){
            suppList!!.add(Support(
                supportId = suppList!!.last().supportId+1,
                agentName = "various",
                requestId = suppList!!.first().requestId,
                essayId = suppList!!.last().essayId,
                workFlowId = suppList!!.first().requestId,
                cropId = suppList!!.first().cropId,
                numOrderId = suppList!!.first().numOrderId,
                activityId = suppList!!.last().activityId
            ))
        }
        val slotTemp = Slot(
            labProcessId = slot.labProcessId,
            agentId = slot.agentId,
            sampleId = slot.sampleId,

            supportId = custSupps!!.last().supportId,
            cellPosition = newCell,
            result = slot.result,
            essayId = suppList!!.last().essayId,
            type = "sample",
            cropId = slot.cropId,
            workflowId = slot.workflowId,
            activityId = suppList!!.last().activityId,
            numOrderId = suppList!!.last().numOrderId
        )
        slotList!!.add(slotTemp)
    }

    private fun genCustEssayPack(slotList: ArrayList<Slot>): ArrayList<Slot>{
        val emptyList = slotList.filter { it.type == "empty" }
        val rduList = slotList - emptyList
        return rduList as ArrayList<Slot>
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    }
}
