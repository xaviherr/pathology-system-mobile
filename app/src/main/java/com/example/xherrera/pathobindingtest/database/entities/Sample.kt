package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["sampleId", "requestId"])
data class Sample(
    @SerializedName("sampleId") @Expose val sampleId: Int,
    @SerializedName("accessionCode") @ColumnInfo(name = "accessionCode") @Expose val accessionCode: String?,
    @SerializedName("collectingCode") @ColumnInfo(name = "collectingCode") @Expose val collectingCode: String?,
    @SerializedName("sampleTypeId") @ColumnInfo(name = "sampleTypeId") @Expose val sampleTypeId: Int,
    @SerializedName("requestId") @ColumnInfo(name = "requestId") @Expose val requestId: Int,
    @SerializedName("numOrderId") @ColumnInfo(name = "numOrderId") val numOrderId: Int?,
    @SerializedName("numOrder") @ColumnInfo(name = "numOrder") val numOrder: Int?
)