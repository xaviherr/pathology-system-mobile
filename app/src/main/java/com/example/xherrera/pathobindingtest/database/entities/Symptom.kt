package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

@Entity
data class Symptom(
    @Expose @PrimaryKey val symptomId: Int,
    @Expose val shortName: String?,
    @Expose val longName: String?,
    @Expose var plantPositionId: Int? = 0,
    @Expose var isSelected: Int? = 0
)