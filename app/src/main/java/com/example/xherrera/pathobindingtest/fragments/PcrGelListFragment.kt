package com.example.xherrera.pathobindingtest.fragments

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.PcrGelListAdapter
import com.example.xherrera.pathobindingtest.adapters.SuppDashAdapter
import com.example.xherrera.pathobindingtest.database.entities.Activity
import com.example.xherrera.pathobindingtest.database.entities.Support
import com.example.xherrera.pathobindingtest.databinding.FragmentPcrGelListBinding
import com.example.xherrera.pathobindingtest.viewmodels.EssayViewModel
import com.google.android.material.button.MaterialButtonToggleGroup
import kotlinx.android.synthetic.main.fragment_pcr_gel_list.view.*
import kotlinx.android.synthetic.main.fragment_sup_dashboard.*


class PcrGelListFragment: Fragment(), PcrGelListAdapter.Listener {

    private lateinit var supportList: ArrayList<Support>
    private var reqTotSup: Int? = 0
    private var numOrderId: Int? = 0
    private var requestId: Int = 0
    private var essayId: Int = 0
    private var activityId: Int = 24
    private lateinit var rqCode: String
    private val activityList: MutableList<Activity> = mutableListOf()
    private lateinit var essayViewModel: EssayViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var toggleGroup: MaterialButtonToggleGroup

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentPcrGelListBinding>(
            inflater,
            R.layout.fragment_pcr_gel_list,
            container,
            false
        )

        val args = PcrSuppSetupFragmentArgs.fromBundle(arguments!!)
        requestId = args.rqId
        essayId = args.essayId
        rqCode = args.rqCode
        numOrderId = args.numOrderId
        var essayName = ""

        if (args.essayId == 8) {
            activityId = 24
            essayName = "PCR"
        }

        binding.tvRqInfo.text = "Request $rqCode | Essay: $essayName"

        toggleGroup = binding.btnGroupActivity

        essayViewModel = ViewModelProviders.of(this).get(EssayViewModel::class.java)
        recyclerView = binding.root.rv_gel_list

        essayViewModel.getRequestData(requestId, essayId, numOrderId!!)

        println("Activities: ${essayViewModel.essayActivities.value}")

        essayViewModel.essayActivities.observe(viewLifecycleOwner, Observer {
            it.forEach { activity -> activityList.add(activity); println("Activity: $activity") }
        })

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.itemAnimator = DefaultItemAnimator()
        essayViewModel.essaySupports.observe(this, Observer {
            supportList = it.filter { supp -> supp.activityId == activityId } as ArrayList<Support>
            recyclerView.adapter = PcrGelListAdapter(supportList, this)
        })

        val toggleListener = MaterialButtonToggleGroup.OnButtonCheckedListener { _, _, _ ->
            when {
                btnAct01.isChecked -> {
                    activityId = activityList[0].activityId
                    println("Activity selected $activityId")
                    println("Support List: $supportList")
                    essayViewModel.essayPack.observe(this, Observer {
                        recyclerView.adapter = PcrGelListAdapter(it.essSupports.filter {
                                support -> support.activityId == activityId },
                            this)
                    })
                    recyclerView.adapter!!.notifyDataSetChanged()
                }
                btnAct02.isChecked -> {
                    activityId = activityList[1].activityId
                    println("Activity selected $activityId")
                    println("Support List: $supportList")
                    essayViewModel.essayPack.observe(this, Observer {
                        recyclerView.adapter = PcrGelListAdapter(it.essSupports.filter {
                                support -> support.activityId == activityId },
                            this)
                    })
                    recyclerView.adapter!!.notifyDataSetChanged()
                }
            }
        }

        toggleGroup.addOnButtonCheckedListener(toggleListener)

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onItemClick(support: Support) {
        essayViewModel.getEssayPack(support.requestId, support.essayId, support.numOrderId)

        essayViewModel.essaySlots.observe(this.viewLifecycleOwner, Observer {
            println("Support ID: ${support.supportId}")
            println("Slots at support: ${it.filter { slot -> slot.supportId == support.supportId && slot.activityId == support.activityId }.count()}")
            when{
                it.filter { slot -> slot.supportId == support.supportId && slot.activityId == support.activityId }.isNullOrEmpty() -> findNavController()
                    .navigate(PcrGelListFragmentDirections
                    .actionPcrGelListFragmentToPcrGelSetupFragment(
                        support.requestId, rqCode, support.essayId, support.supportId, support.numOrderId, support.activityId, supportList.count()
                    ))
                else -> findNavController().navigate(PcrGelListFragmentDirections
                    .actionPcrGelListFragmentToPcrGelResultFragment(
                        support.requestId, rqCode, support.essayId, support.supportId, support.numOrderId, support.activityId
                    ))
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}