package com.example.xherrera.pathobindingtest.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class RequestViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RequestViewModel::class.java)){
            return RequestViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}