package com.example.xherrera.pathobindingtest.database.dao

import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.Symptom

@Dao
interface SymptomDao {

    @Query("SELECT * FROM Symptom")
    suspend fun getSymptoms(): List<Symptom>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(symptom: Symptom)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(symptom: Symptom)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(symptomList: List<Symptom>)
}