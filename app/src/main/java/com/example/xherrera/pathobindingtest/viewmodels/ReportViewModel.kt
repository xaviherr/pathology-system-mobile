package com.example.xherrera.pathobindingtest.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.xherrera.pathobindingtest.database.entities.Lecture
import com.example.xherrera.pathobindingtest.database.entities.RqReport
import com.example.xherrera.pathobindingtest.extDb.ApiService

class ReportViewModel: ViewModel() {

    val reqReportList: MutableLiveData<ArrayList<RqReport>> = MutableLiveData()

    private val apiServe by lazy {
        ApiService.create()
    }

    init {
        Log.i("ReportViewModel", "ReporViewModel has been created")
        reqReportList.value = apiServe.getRqReports().execute().body()
    }

}