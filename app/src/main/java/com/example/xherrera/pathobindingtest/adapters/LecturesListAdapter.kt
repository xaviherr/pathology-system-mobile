package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Lecture
import kotlinx.android.synthetic.main.lecture_item.view.*
import kotlinx.android.synthetic.main.rqlist_item.view.*


class LecturesListAdapter(
    private val lectureList: ArrayList<Lecture>
): RecyclerView.Adapter<LecturesListAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lectureList[position], position)
    }

    override fun getItemCount(): Int = lectureList.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.lecture_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view:View): RecyclerView.ViewHolder(view){
        fun bind(
            lecture: Lecture,
            pos: Int
        ){
            itemView.tvSampleCode.text = "Sample ID: ${lecture.sampleId}"
            itemView.tvSuppId.text = "Support ${lecture.supportId}"
            itemView.tvCellPos.text = "Slot: ${lecture.cellPosition}"
            itemView.tvResult.text = lecture.result
            if (itemView.tvResult.text == "positive") itemView.tvResult.setTextColor(Color.RED)
            if (itemView.tvResult.text == "negative") itemView.tvResult.setTextColor(Color.parseColor("#1B5E20"))
            if(pos%2 == 0){
                itemView.setBackgroundColor(Color.parseColor("#eeeeee"))
            }else{itemView.setBackgroundColor(Color.WHITE)}
        }
    }
}