package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["agentId", "requestId", "essayId"])
data class AgentByProcess(
    @Expose @SerializedName("agentId") @ColumnInfo(name = "agentId") val agentId: Int,
    @Expose @SerializedName("agentName") @ColumnInfo(name = "agentName") val agentName: String,
    @Expose @SerializedName("requestId") @ColumnInfo(name = "requestId") val requestId: Int,
    @Expose @SerializedName("essayId") @ColumnInfo(name = "essayId") val essayId: Int,
    @Expose @SerializedName("agentGroupId") @ColumnInfo(name = "agentGroupId") val agentGroupId: Int
)