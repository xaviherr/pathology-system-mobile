package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Slot
import kotlinx.android.synthetic.main.acc_item.view.*
import kotlinx.android.synthetic.main.cell_item.view.tvCell

class AccAdapter(
    private val usedSlots: Int,
    private val lectureList: ArrayList<Slot>,
    private val listener: Listener
): RecyclerView.Adapter<AccAdapter.ViewHolder>() {

    interface Listener {
        fun onItemClick(slot: Slot, position: Int){
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lectureList[position], listener)
        if (position < usedSlots) {
            holder.itemView.isEnabled = false
            holder.itemView.isFocusable = false
            holder.itemView.isFocusableInTouchMode = false
            holder.itemView.cvSampAcc.setCardBackgroundColor(Color.LTGRAY)
        }
    }

    override fun getItemCount(): Int {
        return lectureList.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.acc_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(
            slot: Slot,
            listener: Listener
        ){
            itemView.setOnClickListener { listener.onItemClick(slot, adapterPosition) }
            itemView.tvCell.text = slot.sampleId.toString()
            if (slot.type == "control") itemView.cvSampAcc.setCardBackgroundColor(Color.YELLOW)
            if (slot.type == "blind") itemView.cvSampAcc.setCardBackgroundColor(Color.DKGRAY)
        }
    }
}