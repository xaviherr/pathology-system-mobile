package com.example.xherrera.pathobindingtest.fragments

import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.AccAdapter
import com.example.xherrera.pathobindingtest.adapters.GridAdapter
import com.example.xherrera.pathobindingtest.database.entities.ReadingDataUnit
import com.example.xherrera.pathobindingtest.database.entities.Slot
import com.example.xherrera.pathobindingtest.databinding.FragmentLectureRegBinding
import com.example.xherrera.pathobindingtest.viewmodels.EssayViewModel
import kotlinx.android.synthetic.main.fragment_lecture_reg.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.collections.ArrayList

@ExperimentalCoroutinesApi
class LectureRegFragment : Fragment(), GridAdapter.Listener, AccAdapter.Listener {

    private var recyclerView: RecyclerView ?= null
    private var numOrderId: Int? = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        val binding = DataBindingUtil.inflate<FragmentLectureRegBinding>(inflater,
            R.layout.fragment_lecture_reg, container, false)

        var wholeSlotList: List<Slot>?= null
        var slotList: ArrayList<Slot>? = null

        /**
         * GETTING NAVIGATION ARGUMENTS
         */
        val args = LectureRegFragmentArgs.fromBundle(arguments!!)
        val requestId = args.rqId
        val suppId = args.suppId
        val essayId = args.essayId
        val disabledSlots = 96-args.avSlots
        val activityId = args.activityId
        numOrderId = args.numOrderId

        /**
         * INVOKING ESSAYVIEWMODEL
         */
        val essayViewModel= ViewModelProviders.of(activity!!).get(EssayViewModel::class.java)
        essayViewModel.getEssayPack(requestId, essayId, args.numOrderId)

        /**
         * GETTING THE AGENT NAMES LIST
         */
        val infoTextView = binding.root.tvSuppInfo
        //val agentNames = arrayListOf<String>()

        val agentName = essayViewModel.essaySupports.value!!.filter {
            it.supportId == suppId && it.activityId == activityId
        }.first().agentName

        //for (i in agentList) agentNames.add(i.agentName)

        infoTextView.text = "Request ID : ${args.rqCode} | Support ID: $suppId | Pathogen: $agentName"

        //val infoTextView = binding.root.tvSuppInfo
        //infoTextView.text = "RequestId: $requestId | Support ID: $suppId | Activity ID: $activityId"

        recyclerView = binding.rvGridSupport
        val gridManager = GridLayoutManager(activity, 8, GridLayoutManager.HORIZONTAL, false)

        recyclerView!!.layoutManager = gridManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()

        essayViewModel.essaySlots.observe(this, Observer {
            wholeSlotList = it
            slotList= it.filter { slot -> slot.supportId == suppId && slot.activityId == activityId } as ArrayList<Slot>
            recyclerView!!.adapter = GridAdapter(disabledSlots, slotList!!, this)
        })

        /**
         * SHOW SAMPLES IDs BUTTON
         */
        val btnShowCodes = binding.root.btnShowCodes
        btnShowCodes.setOnClickListener {
            if (btnShowCodes.text == "IDs") {
                recyclerView!!.adapter = AccAdapter(disabledSlots, slotList!!, this)
                btnShowCodes.text = "Results"
            }else{
                recyclerView!!.adapter = GridAdapter(disabledSlots, slotList!!, this)
                btnShowCodes.text = "IDs"
            }
        }

        /**
         * SUPPORT NAVIGATION BUTTONS
         */
        binding.btnPrevious.setOnClickListener {
            if(suppId == essayViewModel.essaySupports.value!!.first().supportId){
                Toast.makeText(this.context, "It's the first support", Toast.LENGTH_SHORT).show()
            }else{
                findNavController()
                    .navigate(LectureRegFragmentDirections
                        .actionLectureRegFragmentSelf(96, args.rqId, suppId-1, essayId, activityId, args.numOrderId, args.agentName, args.rqCode))
            }
        }
        binding.btnNext.setOnClickListener {
            if(suppId == essayViewModel.essaySupports.value!!.last().supportId){
                Toast.makeText(this.context, "It's the last support", Toast.LENGTH_SHORT).show()
            }else{
                findNavController()
                    .navigate(LectureRegFragmentDirections
                        .actionLectureRegFragmentSelf(96, args.rqId, suppId+1, essayId, activityId, args.numOrderId, args.agentName, args.rqCode))
            }
        }

        /**
         * LOCAL SAVE/UPDATE BUTTON ACTION
         */
        binding.btnSaveLectures.setOnClickListener {

            essayViewModel.updateSlots(wholeSlotList!!)
            println(wholeSlotList!!.count())

            /**
             * CREATING THE ALERT DIALOG TO GO TO THE NEXT SUPPORT OR GO BACK
             */
            AlertDialog.Builder(activity!!)
                .setTitle("Local save")
                .setMessage("The results have been saved locally")
                /**
                 * SETTING UP THE NEXT-SUPPORT BUTTON
                 */
                .setPositiveButton("OK", null)
                .create().show()
        }

        /**
         * MYSQL SAVE BUTTON ACTION
         */
        if(suppId == essayViewModel.essaySupports.value!!.last().supportId){
            binding.btnSend.isClickable = true
            binding.btnSend.isFocusable = true
            binding.btnSend.isFocusableInTouchMode = true
            binding.btnSend.setBackgroundColor(Color.BLUE)
            binding.root.btnSend.setOnClickListener {
                /**
                 * CREATING THE ALERT DIALOG TO CONFIRM OR CANCEL
                 */
                AlertDialog.Builder(activity!!)
                    .setTitle("Send results")
                    .setMessage("The results will be sent to the database")
                    /**
                     * SETTING THE CONFIRM BUTTON TO ACTUALLY POST THE RESULTS TO THE API REST
                     */
                    .setPositiveButton("I'VE FINISHED"){_,_ ->
                        //Converting Slots into ReadingDataUnits
                        val rduList: ArrayList<ReadingDataUnit> = ArrayList()
                        essayViewModel.essaySlots.observe(viewLifecycleOwner, Observer {
                            val cropId = it.first().cropId
                            val workflowId = it.first().workflowId
                            val numOrderId = it.first().numOrderId
                            for(i in it){
                                val rduType = if (i.type == "control") {11} else if (i.type == "blind") {12} else if (i.type == "empty") {73} else 13
                                val rdu = ReadingDataUnit(
                                    agentId = i.agentId,
                                    sampleId = i.sampleId,
                                    cellPosition = i.cellPosition,
                                    result = i.result,
                                    supportId = i.supportId,
                                    activityId = i.activityId,
                                    essayId = i.essayId,
                                    requestId = requestId,
                                    cropId = i.cropId,
                                    numOrderId = i.numOrderId,
                                    workFlowId = i.workflowId,
                                    readingDataTypeId = rduType,
                                    symptomId = 1
                                )
                                rduList.add(rdu)
                            }
                            println("RDUs: ${rduList.count()}")
                            //Saving into the Room Database
                            essayViewModel.insertRdus(requestId, essayId, rduList)
                            //Posting through the API rest
                            essayViewModel.workUpdateRduList(requestId, essayId, activityId, numOrderId)
                        })
                    }
                    .setNegativeButton("NOT YET", null)
                    .create().show()
            }
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val args = LectureRegFragmentArgs.fromBundle(arguments!!)
        findNavController().navigate(LectureRegFragmentDirections.actionLectureRegFragmentToSupDashboardFragment(args.rqId, false, args.essayId, args.numOrderId, args.rqCode))
        return true
    }

    override fun onItemClick(slot: Slot, position: Int) {
        when {
            slot.result=="negative" -> slot.result="positive"
            slot.result =="positive" -> slot.result="doubt"
            slot.result == "doubt" -> slot.result="buffer"
            else -> slot.result = "negative"
        }
        recyclerView!!.adapter!!.notifyItemChanged(position)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    }
}
