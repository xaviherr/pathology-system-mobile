package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["requestId", "workFlowId", "essayId"])
data class ReqProcess(
    @ColumnInfo(name = "requestId") @SerializedName("requestId") @Expose val requestId: Int,
    @ColumnInfo(name = "cropName") @SerializedName("cropName") @Expose val cropName: String,
    @ColumnInfo(name = "cropId") @SerializedName("cropId") @Expose val cropId: Int,
    @ColumnInfo(name = "workFlowId") @SerializedName("workFlowId") @Expose val workFlowId: Int,
    @ColumnInfo(name = "essayId") @SerializedName("essayId") @Expose val essayId: Int,
    @ColumnInfo(name = "essayName") @SerializedName("essayName") @Expose val essayName: String,
    @ColumnInfo(name = "sampleQty") @SerializedName("sampleQty") @Expose val sampleQty: Int,
    @ColumnInfo(name = "activityId") @SerializedName("activityId") @Expose val activityId: Int,
    @ColumnInfo(name = "activityName") @SerializedName("activityName") @Expose val activityName: String,
    @ColumnInfo(name = "numOrderId") @SerializedName("numOrderId") @Expose val numOrderId: Int,
    @ColumnInfo(name = "locationName") @SerializedName("locationName") @Expose val locationName: String
)