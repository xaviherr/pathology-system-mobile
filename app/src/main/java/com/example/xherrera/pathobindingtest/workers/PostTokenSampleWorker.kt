package com.example.xherrera.pathobindingtest.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.xherrera.pathobindingtest.extDb.ApiService

class PostTokenSampleWorker(context: Context, params: WorkerParameters): Worker(context, params) {

    private val apiServe by lazy {
        ApiService.create()
    }

    override fun doWork(): Result {
        makeStatusNotification("The results were sent", applicationContext)

        val requestId = inputData.getInt("rqId", 1)
        val cropId = inputData.getInt("cropId", 1)
        val numOrderId = inputData.getInt("numOrderId", 1)
        val workflowId = inputData.getInt("workflowId", 1)

        val response = apiServe.postTokenSample(requestId, cropId, workflowId, numOrderId).execute()

        if (response.isSuccessful){
            return Result.success()
        } else{
            if (response.code() in 500..799){
                return Result.retry()
            }
            return Result.failure()
        }
    }
}