package com.example.xherrera.pathobindingtest.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Slot
import com.example.xherrera.pathobindingtest.database.entities.Support
import kotlinx.android.synthetic.main.dashboard_item.view.*

class SuppDashAdapter(
    private val suppList: List<Support>?,
    slotList: List<Slot>,
    private val listener: Listener
): RecyclerView.Adapter<SuppDashAdapter.ViewHolder>() {

    private var bySupp = slotList.groupBy{ it.supportId } //01-10-2020

    interface Listener {
        fun onItemClick(support: Support)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val slotBySupp = bySupp.values.toMutableList()
        Log.d("slotsGroups", "${slotBySupp.size}")
        holder.bind(suppList!![position], listener, position)
        holder.itemView.rvSuppMin.layoutManager = GridLayoutManager(holder.itemView.rvSuppMin.context, 8, GridLayoutManager.HORIZONTAL, false)
        holder.itemView.rvSuppMin.itemAnimator = DefaultItemAnimator()
        var adapter = ChildDashAdapter(slotBySupp[position] as ArrayList<Slot>)
        holder.itemView.rvSuppMin.adapter = adapter
    }

    override fun getItemCount(): Int {
        return suppList?.count() ?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dashboard_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        fun bind(
            support: Support,
            listener: Listener,
            pos:Int
        ){
            val agentName = support.agentName
            val suppId = support.supportId
            val avSlots = support.availableSlots
            val activityId = support.activityId
            val supportName = if (support.essayId == 1) {
                "Plate"
            } else if ( support.essayId == 4 || support.essayId == 5 ) {
                "Membrane"
            } else "Diagram"
            itemView.btnRegLectures.setOnClickListener { listener.onItemClick(support) }
            itemView.rvSuppMin.itemAnimator = DefaultItemAnimator()
            itemView.tvSuppId.text = "$supportName: $suppId"
            itemView.tvSuppPatho.text = "Pathogen: $agentName"
            //itemView.tvSuppAvSlots.text = "Activity Id: $activityId"
        }
    }
}