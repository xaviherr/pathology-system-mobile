package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.Entity
import com.google.gson.annotations.Expose

@Entity(primaryKeys = ["plantId", "sampleId", "requestId", "essayId", "activityId"])
data class PlantBySample(
    @Expose val plantId: Int,
    @Expose val plantName: String,
    @Expose val sampleId: Int,
    @Expose val sampCorrelative: Int,
    @Expose val requestId: Int,
    @Expose val essayId: Int,
    @Expose val insertTypeId: Int,
    @Expose val activityId: Int,
    @Expose val result: String = "negative",
    @Expose val numOrderId: Int,
    @Expose val workflowId: Int,
    @Expose val agentId: Int,
    @Expose val cropId: Int
)