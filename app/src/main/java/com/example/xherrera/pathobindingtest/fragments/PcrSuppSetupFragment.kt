package com.example.xherrera.pathobindingtest.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.AgentByProcess
import com.example.xherrera.pathobindingtest.database.entities.Sample
import com.example.xherrera.pathobindingtest.databinding.FragmentPcrSuppSetupBinding
import com.example.xherrera.pathobindingtest.viewmodels.EssayViewModel
import com.example.xherrera.pathobindingtest.viewmodels.RequestViewModel
import kotlinx.android.synthetic.main.fragment_pcr_supp_setup.view.*
import kotlinx.android.synthetic.main.fragment_supports_setup.view.tvRqInfo

class PcrSuppSetupFragment: Fragment() {

    private lateinit var sampList: ArrayList<Sample>
    private lateinit var agentList: ArrayList<AgentByProcess>
    private var reqTotSup: Int? = 0
    private var numOrderId: Int? = 0
    private lateinit var requestViewModel: RequestViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentPcrSuppSetupBinding>(
            inflater,
            R.layout.fragment_pcr_supp_setup,
            container,
            false
        )

        requestViewModel = ViewModelProviders.of(this).get(RequestViewModel::class.java)

        val args = PcrSuppSetupFragmentArgs.fromBundle(arguments!!)
        val requestId = args.rqId
        val essayId = args.essayId
        val rqCode = args.rqCode
        numOrderId = args.numOrderId

        binding.root.tvRqInfo.text = "Request $rqCode"

        requestViewModel.sampleListLiveData.observe(this, Observer{
            sampList = it as ArrayList<Sample>
        })
        requestViewModel.agentByProcessList.observe(this, Observer { agents ->
            agentList = agents.filter { it.essayId == essayId } as ArrayList<AgentByProcess>
        })

        val essayViewModel = ViewModelProviders.of(this).get(EssayViewModel::class.java)
        essayViewModel.getRequestData(requestId, essayId, numOrderId!!)
        println("Activities: ${essayViewModel.essayActivities.value}")

        essayViewModel.essaySamples.observe(this, Observer {
            binding.insSampleQty.setText(it.count().toString())
        })
        essayViewModel.essayAgents.observe(this, Observer{
            binding.insPathoQty.setText(it.count().toString())
        })

        binding.btnProceed.setOnClickListener { v: View ->

            val supportQty = binding.root.insGelQty.text.toString().toInt()

            val essayBrief = essayViewModel.createPcrEssayBrief(supportQty)
            val supports = essayViewModel.newGenPcrSupports(essayBrief)

            reqTotSup = supports.count()

            AlertDialog.Builder(activity)
                .setTitle("$reqTotSup gel supports generated")
                .setPositiveButton("OK") { _,_ ->
                    v.findNavController().navigate(PcrSuppSetupFragmentDirections.
                    actionPcrSuppSetupFragmentToPcrGelListFragment(args.pathogenQty, requestId, rqCode, essayId, numOrderId!!))
                }
                .create().show()
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}