package com.example.xherrera.pathobindingtest.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xherrera.pathobindingtest.database.entities.ReqProcess

class EssayViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EssayViewModel::class.java)){
            return EssayViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}