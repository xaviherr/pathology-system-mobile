package com.example.xherrera.pathobindingtest.database


import android.util.Log
import com.example.xherrera.pathobindingtest.database.entities.*
import com.example.xherrera.pathobindingtest.extDb.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class PathoRepo(private val apiServe: ApiService, private val dBase: PathoDatabase) {

    suspend fun getRequests(): List<Request>?{
        return apiServe.getRequestsAsync().await()
    }

    suspend fun getSamples(requestId: Int, numOrderId: Int): List<Sample>?{
        return apiServe.getSamples(requestId, numOrderId).await()
    }

    suspend fun getAgentsByProcess(requestId: Int): List<AgentByProcess>?{
        return apiServe.getAgentsByProcess(requestId).await()
    }

    suspend fun getActivities(): List<Activity>{
        return apiServe.getActivities().await()
    }

    suspend fun getServerSupportsByEssay(requestId: Int, essayId: Int): List<Support>?{
        return apiServe.getSupports(requestId, essayId).execute().body()
    }

    suspend fun postSupports(supportList: List<Support>){
        withContext(Dispatchers.IO){
            apiServe.postSuppArray(supportList)
        }
    }

    suspend fun deleteLocalRequests(){
        withContext(Dispatchers.IO){
            dBase.requestDao().deleteAll()
        }
    }

    suspend fun getSlots(requestId: Int, essayId: Int): List<PreSlot>?{
        return apiServe.getSlots(requestId, essayId).execute().body()
    }

    suspend fun getLocalPlants(requestId: Int, essayId: Int): List<PlantBySample>{
        return dBase.plantBySampleDao().getEssayPlants(requestId, essayId)
    }

    suspend fun getLocalActivities(): List<Activity>{
        return dBase.activityDao().getActivityList()
    }

    suspend fun deleteLocalActivities(){
        withContext(Dispatchers.IO){
            dBase.activityDao().deleteAll()
        }
    }

    suspend fun getLocalActivitiesByEssay(essayId: Int): List<Activity>{
        return dBase.activityDao().getActivitiesByEssay(essayId)
    }

    suspend fun getLocalPlantSlots(requestId: Int, essayId: Int): List<PlantSlot>{
        return dBase.plantSlotDao().getEssayPlantRdus(requestId, essayId)
    }

    suspend fun getSpecificPlantSlots(plantId: Int, requestId: Int, essayId: Int, activityId: Int): List<PlantSlot>{
        return dBase.plantSlotDao().getPlantRduByPlant(plantId, requestId, essayId, activityId)
    }

    suspend fun getLocalRdusByEssay(requestId: Int, essayId: Int): List<ReadingDataUnit>?{
        return dBase.readingDataUnitDao().getEssayRdus(requestId, essayId)
    }

    suspend fun getSpecificRdus(requestId: Int, essayId: Int, activityId: Int, numOrderId: Int, supportId: Int): List<ReadingDataUnit>?{
        return dBase.readingDataUnitDao().getSpecificRdusBySupport(requestId, essayId, activityId, numOrderId, supportId)
    }

    suspend fun getSlotsByEssay(requestId: Int, essayId: Int): List<Slot>{
        return dBase.slotDao().getEssaySlots(requestId, essayId)
    }

    suspend fun getSupportsByEssay(requestId: Int, essayId: Int): List<Support>{
        return dBase.supportDao().getEssaySupports(requestId, essayId)
    }

    suspend fun getLocalAgents(requestId: Int): List<AgentByProcess>{
        return dBase.agentByProcessDao().getAgentsWhere(requestId)
    }

    fun updateSupport(requestId: Int, essayId: Int, activityId: Int, supportId: Int, usedSlots: Int, ctrlSlots: Int){
        return dBase.supportDao().updateSupport(requestId = requestId, essayId = essayId, activityId = activityId, usedSlots = usedSlots, ctrlSlots = ctrlSlots, supportId = supportId)
    }

    fun getLocalAgentsEager(requestId: Int): List<AgentByProcess>{
        return dBase.agentByProcessDao().getAgentsWhereEager(requestId)
    }

    suspend fun getLocalAgentsByEssay(requestId: Int, essayId: Int): List<AgentByProcess>{
        return dBase.agentByProcessDao().getAgentsByEssay(requestId, essayId)
    }

    suspend fun getLocalSamples(requestId: Int, numOrderId: Int): List<Sample>{
        return dBase.sampleDao().getSampWhere(requestId, numOrderId)
    }

    fun getLocalSamplesEager(requestId: Int): List<Sample>{
        return dBase.sampleDao().getSamples(requestId)
    }

    suspend fun getLocalSymptomList(): List<Symptom>{
        return dBase.symptomDao().getSymptoms()
    }

    suspend fun getSymptomList(): List<Symptom>{
        return apiServe.getSymptoms().await()
    }

    suspend fun getLocalRequests(): List<Request>?{
        return dBase.requestDao().getRequestListAsync()
    }

    suspend fun getLocalReqDetail(requestId: Int): List<ReqProcess>{
        return dBase.reqProcessDao().getReqProcWhere(requestId)
    }

    suspend fun getLocalSpecificReqDetail(requestId: Int, essayId: Int): ReqProcess{
        return dBase.reqProcessDao().getSpecificReqProc(requestId, essayId)
    }

    suspend fun getRqDetails(requestId: Int, numOrderId: Int): ArrayList<ReqProcess>?{
        return apiServe.getReqDetails(requestId, numOrderId).await()
    }

    suspend fun insertSupports(supportList: List<Support>){
        dBase.supportDao().insertAll(supportList)
    }

    suspend fun insertPlants(plantList: List<PlantBySample>){
        dBase.plantBySampleDao().insertAll(plantList)
    }

    suspend fun insertPlantSlots(plantSlotList: List<PlantSlot>){
        plantSlotList.forEach {
            dBase.plantSlotDao().insert(it)
        }
        //dBase.plantSlotDao().insertAll(plantSlotList)
    }

    suspend fun insertActivities(activityList: List<Activity>){
        dBase.activityDao().insertAll(activityList)
    }

    suspend fun insertSymptoms(symptomList: List<Symptom>){
        dBase.symptomDao().insertAll(symptomList)
    }

    suspend fun insertSlots(slotList: List<Slot>){
        dBase.slotDao().insertAll(slotList)
    }

    suspend fun insertRdus(rduList: ArrayList<ReadingDataUnit>){
        dBase.readingDataUnitDao().insertAll(rduList)
    }

    suspend fun insertAgents(agentList: List<AgentByProcess>){
        dBase.agentByProcessDao().insertAll(agentList)
    }

    suspend fun insertReqProcesses(reqProcessList: List<ReqProcess>){
        dBase.reqProcessDao().insertAll(reqProcessList)
    }

    suspend fun insertSamples(sampleList: List<Sample>){
        dBase.sampleDao().insertAll(sampleList)
    }

    suspend fun insertRequests(requestList: List<Request>){
        dBase.requestDao().insertAllAsync(requestList)
    }

    suspend fun updateRdus(rduList: List<ReadingDataUnit>){
        dBase.readingDataUnitDao().updateAll(rduList)
    }

    suspend fun updateSlots(slotList: List<Slot>){
        dBase.slotDao().updateAll(slotList)
    }

    suspend fun updatePlantSlots(plantSlotList: List<PlantSlot>){
        dBase.plantSlotDao().updateAll(plantSlotList)
    }

    fun deleteLocalRequestData(requestId: Int){
        dBase.reqProcessDao().deleteProcesses(requestId)
        dBase.sampleDao().deleteSamples(requestId)
        dBase.agentByProcessDao().deleteAgents(requestId)
        dBase.supportDao().deleteSupports(requestId)
        dBase.plantBySampleDao().deletePlants(requestId)
        dBase.slotDao().deleteSlots(requestId)
        dBase.plantSlotDao().deletePlantSlots(requestId)
        dBase.readingDataUnitDao().deleteRdus(requestId)
    }

    fun deleteLocalEssayData(requestId: Int, essayId: Int){
        dBase.supportDao().deleteSupportsByEssay(requestId, essayId)
        dBase.plantBySampleDao().deletePlantsByEssay(requestId, essayId)
        dBase.slotDao().deleteSlotsByEssay(requestId, essayId)
        dBase.plantSlotDao().deletePlantSlotsByEssay(requestId, essayId)
        dBase.readingDataUnitDao().deleteRdusByEssay(requestId, essayId)
    }

    suspend fun clearDb(){
        withContext(Dispatchers.Default){
            dBase.clearAllTables()
            Log.i("Database", "Database cleared")
        }
    }
}