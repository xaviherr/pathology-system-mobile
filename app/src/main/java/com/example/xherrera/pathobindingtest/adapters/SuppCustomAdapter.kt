package com.example.xherrera.pathobindingtest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Slot
import com.example.xherrera.pathobindingtest.database.entities.Support
import kotlinx.android.synthetic.main.dashboard_item.view.*

class SuppCustomAdapter(private val suppList: ArrayList<Support>?,
                        slotList: ArrayList<Slot>,
                        private val listener: Listener,
                        private val childListener: ChildCustomAdapter.Listener //chequear
): RecyclerView.Adapter<SuppCustomAdapter.ViewHolder>() {

    private val bySupp = slotList.groupBy { it.supportId }

    interface Listener {
        fun onItemClick(support: Support, position: Int, slotList: ArrayList<Slot>)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val slotBySupp = bySupp.values.toMutableList()
        holder.bind(suppList!![position], listener, position, slotBySupp as ArrayList<Slot>)
        holder.itemView.rvSuppMin.layoutManager = GridLayoutManager(holder.itemView.rvSuppMin.context, 8, GridLayoutManager.HORIZONTAL, false)
        holder.itemView.rvSuppMin.itemAnimator = DefaultItemAnimator()
        var adapter = ChildCustomAdapter(slotBySupp[position] as ArrayList<Slot>, childListener) //chequear
        holder.itemView.rvSuppMin.adapter = adapter
    }

    override fun getItemCount(): Int {
        return suppList?.count() ?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        fun bind(
            support: Support,
            listener: Listener,
            position: Int,
            slotList: ArrayList<Slot>
        ){
            val suppId = support.supportId.toString()
            val avSlots = support.availableSlots.toString()
            itemView.setOnClickListener { listener.onItemClick(support, position, slotList) }
            itemView.rvSuppMin.itemAnimator = DefaultItemAnimator()
            itemView.tvSuppId.text = "Diagram: $suppId"
            itemView.tvSuppPatho.text = "Pathogen: PVX"
            itemView.tvSuppAvSlots.text = "Total Slots: $avSlots"
        }
    }
}