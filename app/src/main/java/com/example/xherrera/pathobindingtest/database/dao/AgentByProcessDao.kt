package com.example.xherrera.pathobindingtest.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.AgentByProcess

@Dao
interface AgentByProcessDao {

    @Query("SELECT * FROM AgentByProcess")
    fun getAgents(): LiveData<AgentByProcess>

    @Query("SELECT * FROM AgentByProcess")
    suspend fun getAgentList(): List<AgentByProcess>

    @Query("SELECT * FROM AgentByProcess WHERE requestId = :requestId")
    suspend fun getAgentsWhere(requestId: Int): List<AgentByProcess>

    @Query("SELECT * FROM AgentByProcess WHERE requestId = :requestId")
    fun getAgentsWhereEager(requestId: Int): List<AgentByProcess>

    @Query("SELECT * FROM AgentByProcess WHERE requestId = :requestId AND essayId = :essayId")
    suspend fun getAgentsByEssay(requestId: Int, essayId: Int): List<AgentByProcess>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(agentByProcess: AgentByProcess)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(agentByProcess: AgentByProcess)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(agentByProcessList: List<AgentByProcess>)

    @Query("DELETE FROM AgentByProcess WHERE requestId = :requestId")
    fun deleteAgents(requestId: Int)
}