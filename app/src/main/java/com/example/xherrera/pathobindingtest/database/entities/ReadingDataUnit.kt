package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["cellPosition", "supportId", "essayId", "requestId", "activityId", "symptomId", "agentId"])
data class ReadingDataUnit(
    @SerializedName("agentId") @Expose @ColumnInfo(name = "agentId") val agentId: Int = 16,
    @SerializedName("sampleId") @Expose @ColumnInfo(name = "sampleId") val sampleId: Int = 15,
    @SerializedName("cellPosition") @Expose @ColumnInfo(name = "cellPosition") val cellPosition: Int,
    @SerializedName("result") @Expose @ColumnInfo(name = "result") var result: String ?= "negative",
    @SerializedName("supportId") @Expose @ColumnInfo(name = "supportId") val supportId: Int = 1,
    @SerializedName("activityId") @Expose @ColumnInfo(name = "activityId") val activityId: Int,
    @SerializedName("essayId") @Expose @ColumnInfo(name = "essayId") val essayId: Int,
    @SerializedName("requestId") @Expose @ColumnInfo(name = "requestId") val requestId: Int,
    @SerializedName("cropId") @Expose @ColumnInfo(name = "cropId") var cropId: Int?,
    @SerializedName("numOrderId") @Expose @ColumnInfo(name = "numOrderId") val numOrderId: Int,
    @SerializedName("workFlowId") @Expose @ColumnInfo(name = "workFlowId") var workFlowId: Int,
    @SerializedName("symptomId") @Expose @ColumnInfo(name = "symptomId") val symptomId: Int,
    @SerializedName("readingDataTypeId") @Expose @ColumnInfo(name = "readingDataTypeId") val readingDataTypeId: Int = 13
)