package com.example.xherrera.pathobindingtest.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.Slot

@Dao
interface SlotDao {

    @Query("SELECT * FROM Slot")
    fun getSlots(): LiveData<List<Slot>>

    @Query("SELECT * FROM Slot WHERE requestId = :requestId AND essayId = :essayId ORDER BY cellPosition")
    suspend fun getEssaySlots(requestId: Int, essayId: Int): List<Slot>

    @Query("SELECT * FROM Slot WHERE requestId = :requestId AND essayId = :essayId AND supportId = :supportId")
    suspend fun getSupportSlots(requestId: Int, essayId: Int, supportId: Int): List<Slot>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(slot: Slot)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateAll(slotList: List<Slot>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(slot: Slot)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(slotList: List<Slot>)

    @Query("DELETE FROM Slot WHERE requestId = :requestId")
    fun deleteSlots(requestId: Int)

    @Query("DELETE FROM Slot WHERE requestId = :requestId AND essayId = :essayId")
    fun deleteSlotsByEssay(requestId: Int, essayId: Int)
}