package com.example.xherrera.pathobindingtest.fragments


import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.ReportListAdapter
import com.example.xherrera.pathobindingtest.database.entities.RqReport
import com.example.xherrera.pathobindingtest.databinding.FragmentReportsBinding
import com.example.xherrera.pathobindingtest.viewmodels.ReportViewModel


class ReportsFragment : Fragment(), ReportListAdapter.Listener {

    private var manager: RecyclerView.LayoutManager? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentReportsBinding>(
            inflater, R.layout.fragment_reports, container, false
        )

        val reportViewModel = ViewModelProviders.of(this).get(ReportViewModel::class.java)
        val reportList = reportViewModel.reqReportList.value

        val recyclerView = binding.rvReports
        manager = LinearLayoutManager(activity)
        recyclerView.layoutManager = manager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = ReportListAdapter(reportList!!, this)

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item,
            view!!.findNavController()
        )
                || super.onOptionsItemSelected(item)
    }

    override fun onItemClick(rqReport: RqReport) {

        return findNavController().navigate(ReportsFragmentDirections.actionReportsFragmentToNewReportFragment(
            rqReport.requestId,
            rqReport.essayId,
            rqReport.agentId,
            rqReport.essayName,
            rqReport.agentName
        ))

    }
}
