package com.example.xherrera.pathobindingtest.fragments


import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.databinding.FragmentBioEssaySetupBinding
import com.example.xherrera.pathobindingtest.viewmodels.GreenHouseViewModel
import kotlinx.android.synthetic.main.fragment_bio_essay_setup.view.*

class BioEssaySetupFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentBioEssaySetupBinding>(
            inflater,
            R.layout.fragment_bio_essay_setup,
            container,
            false
        )

        /**
         * GETTING NAVIGATION ARGUMENTS
         */
        val args = BioEssaySetupFragmentArgs.fromBundle(arguments!!)
        val requestId = args.rqId
        val essayId = args.essayId
        val rqCode = args.rqCode
        val numOrderId = args.numOrderId

        binding.root.tvRqInfo.text = "Request: $rqCode"

        /**
         * INVOKING THE VIEWMODEL AND GETTING THE REQUEST PROCESS DATA
         */
        val viewModel = ViewModelProviders.of(this).get(GreenHouseViewModel::class.java)
        viewModel.getRequestData(requestId, essayId, numOrderId)
        viewModel.ghSamples.observe(this, Observer {
            binding.insSampleQty.setText(it.count().toString())
        })

        /**
         * ASSIGNING HOST QTY (CHANGE THIS FOR DATA BINDING)
         */
        if (essayId == 3){
            binding.insHostQty.setText("3")
        } else if (essayId == 2) {
            binding.insHostQty.setText("13")
        }

        /**
         * BUTTON CREATE DISTRIBUTION ACTION
         */
        binding.btnProceed.setOnClickListener { v: View ->
            val sampleQty = binding.insSampleQty.text.toString().toInt()
            val hostQty = binding.insHostQty.text.toString().toInt()

            val bioPack = viewModel.createBioPack(sampleQty, hostQty, 1) //CORRECT THE INSERTTYPEID
            println("BioPack created: ${bioPack.bioEssPlantSlots.count()}")
            viewModel.fillBioPack(bioPack)

            AlertDialog.Builder(activity!!)
                .setIcon(R.drawable.ic_leaf_gray)
                .setTitle("Visual diagram")
                .setMessage("Select a plant icon to enter data")
                .setPositiveButton("OK"){_,_ ->
                    v.findNavController().
                    navigate(BioEssaySetupFragmentDirections.
                    actionBioEssaySetupFragmentToBioEssayDashFragment(
                        requestId, rqCode, essayId, numOrderId))
                }
                .create().show()
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}
