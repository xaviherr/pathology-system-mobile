package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.PlantBySample
import kotlinx.android.synthetic.main.bio_dash_item.view.*

class BioDashAdapter(
    private val plantList: List<PlantBySample>,
    private val listener: Listener
): RecyclerView.Adapter<BioDashAdapter.ViewHolder>() {

    interface Listener{
        fun onItemClick(plant: PlantBySample, position: Int){
        }
    }

    override fun getItemCount(): Int {
        return plantList.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(plantList[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.bio_dash_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(
            plant: PlantBySample,
            listener: Listener
        ){
            itemView.setOnClickListener { listener.onItemClick(plant, adapterPosition) }
            itemView.tvSample.text = plant.sampCorrelative.toString()
            itemView.tvHost.text = plant.plantId.toString()
            //itemView.tvAct.text = plant.activityId.toString()
            if (plant.plantId == 1) itemView.imgDiagnosis.setColorFilter(Color.parseColor("#9E9D24"))
            if (plant.plantId == 13) itemView.imgDiagnosis.setColorFilter(Color.parseColor("#A1887F"))
            if (plant.result != "negative") itemView.imgDiagnosis.setImageResource(R.drawable.ic_plant_red)
            if (plant.sampleId % 2 == 0) itemView.cvBioItem.setCardBackgroundColor(Color.parseColor("#4DD0E1"))
        }
    }
}