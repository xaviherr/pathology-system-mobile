package com.example.xherrera.pathobindingtest.fragments


import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import android.os.Environment.getExternalStoragePublicDirectory
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.BioDashAdapter
import com.example.xherrera.pathobindingtest.adapters.SymptomCheckAdapter
import com.example.xherrera.pathobindingtest.database.entities.*
import com.example.xherrera.pathobindingtest.databinding.FragmentBioEssayDashBinding
import com.example.xherrera.pathobindingtest.viewmodels.GreenHouseViewModel
import com.google.android.material.button.MaterialButtonToggleGroup
import kotlinx.android.synthetic.main.dialog_symptoms.view.*
import kotlinx.android.synthetic.main.fragment_bio_essay_dash.*
import kotlinx.android.synthetic.main.fragment_bio_essay_dash.btnAct01
import kotlinx.android.synthetic.main.fragment_bio_essay_dash.btnAct02
import kotlinx.android.synthetic.main.fragment_bio_essay_dash.btnAct03
import kotlinx.android.synthetic.main.fragment_bio_essay_dash.btnAct04
import kotlinx.android.synthetic.main.fragment_bio_essay_dash.btnAct05
import kotlinx.android.synthetic.main.fragment_bio_essay_dash.view.*
import kotlinx.android.synthetic.main.fragment_sup_dashboard.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class BioEssayDashFragment : Fragment(), BioDashAdapter.Listener, SymptomCheckAdapter.Listener {

    private var recyclerView: RecyclerView? = null
    private val activityList: MutableList<Activity> = mutableListOf()
    private val selectedSymptoms = mutableListOf<Symptom>()
    private var rvSymptoms: RecyclerView? = null
    private lateinit var ghViewModel: GreenHouseViewModel
    lateinit var currentPhotoPath: String
    private val REQUEST_TAKE_PHOTO = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentBioEssayDashBinding>(
            inflater,
            R.layout.fragment_bio_essay_dash,
            container,
            false
        )

        val args = BioEssayDashFragmentArgs.fromBundle(arguments!!)
        val requestId = args.rqId
        val essayId = args.essayId
        val rqCode = args.rqCode

        binding.tvRqInfo.text = "Request: $rqCode"

        val toggleGroup = binding.root.btnGroupActivity

        var activityId = 4

        ghViewModel = ViewModelProviders.of(this).get(GreenHouseViewModel::class.java)
        ghViewModel.getBioPack(requestId, essayId, args.numOrderId)

        println(ghViewModel.ghSymptoms.value)
        ghViewModel.ghActivities.observe(this, Observer {
            it.filter { act -> !(act.activityName.toLowerCase().contains("photo")) }.forEach {
                    activity -> activityList.add(activity);
            }
        })

        val columnCount = 13

        recyclerView = binding.rvPlants
        val gridManager = GridLayoutManager(activity, columnCount, RecyclerView.VERTICAL, false)
        recyclerView!!.layoutManager = gridManager
        recyclerView!!.setItemViewCacheSize(100)
        recyclerView!!.itemAnimator = DefaultItemAnimator()

        /**
         * INITIATING THE ACTIVITY FILTERS FOR THE PLANTS LIST TO FEED THE RECYCLER VIEW
         */
        var sampleFilter: CharSequence?
        var displayPlantList = listOf<PlantBySample>()

        ghViewModel.ghPlants.observe(this, Observer {
            displayPlantList = it.filter {plant -> plant.activityId == activityId}
            recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
        })

        val toggleListener = MaterialButtonToggleGroup.OnButtonCheckedListener { _, _, _ ->
            when {
                btnAct01.isChecked -> {
                    activityId = activityList[0].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
                btnAct02.isChecked -> {
                    activityId = activityList[1].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
                btnAct03.isChecked -> {
                    activityId = activityList[2].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
                btnAct04.isChecked -> {
                    activityId = activityList[3].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
                btnAct05.isChecked -> {
                    activityId = activityList[4].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
                btnAct06.isChecked -> {
                    activityId = activityList[5].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
                btnAct07.isChecked -> {
                    activityId = activityList[6].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
                btnAct08.isChecked -> {
                    activityId = activityList[7].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
                btnAct09.isChecked -> {
                    activityId = activityList[8].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
                btnAct10.isChecked -> {
                    activityId = activityList[9].activityId
                    ghViewModel.ghBioPack.observe(this, Observer {
                        displayPlantList = it.bioEssPlants.filter { plant -> plant.activityId == activityId}
                        recyclerView!!.adapter = BioDashAdapter(displayPlantList, this)
                    })
                    recyclerView!!.adapter!!.notifyDataSetChanged()
                }
            }
        }
        toggleGroup.addOnButtonCheckedListener(toggleListener)

        /**
         * SAMPLE ID FILTER FOR THE PLANTS DASHBOARD
         */
        binding.insRqBrowser.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                sampleFilter = p0
                val currentDisplayPlantList = displayPlantList.filter { it.sampCorrelative.toString().contains(p0!!) }
                recyclerView!!.adapter = BioDashAdapter(currentDisplayPlantList, this@BioEssayDashFragment)
                recyclerView!!.adapter!!.notifyDataSetChanged()
            }
        })

        //ghViewModel.insertSymptoms()

        binding.fabSendResults.setOnClickListener {
            ghViewModel.getBioPack(requestId, essayId, args.numOrderId)
            AlertDialog.Builder(activity!!)
                .setTitle("Send Results")
                .setMessage("The results will be sent to the database")
                .setPositiveButton("I'VE FINISHED"){_,_ ->
                    val rduList = arrayListOf<ReadingDataUnit>()
                    ghViewModel.ghSlots.observe(viewLifecycleOwner, Observer {
                        it.filter { plantSlot -> plantSlot.activityId == activityId }.forEach { plantSlot ->
                            val rdu = ReadingDataUnit(
                                agentId = plantSlot.agentId,
                                sampleId = plantSlot.sampleId,
                                cellPosition = plantSlot.plantPosition,
                                result = plantSlot.result,
                                supportId = plantSlot.plantId,
                                activityId = plantSlot.activityId,
                                essayId = plantSlot.essayId,
                                requestId = plantSlot.requestId,
                                cropId = plantSlot.cropId,
                                numOrderId = plantSlot.numOrderId,
                                workFlowId = plantSlot.workflowId,
                                symptomId = plantSlot.symptomId
                            )
                            rduList.add(rdu)
                        }
                        println("RDUs: ${rduList.count()}")
                        //Saving into the Room Database
                        ghViewModel.insertRdus(requestId, essayId, rduList)
                        //Posting through the API rest
                        ghViewModel.workPostRduList(requestId, essayId, activityId)
                    })
                }
                .setNegativeButton("NOT YET", null)
                .create().show()
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onItemClick(plant: PlantBySample, position: Int) {

        println(plant)

        val dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_symptoms, null)
        val builder = AlertDialog.Builder(activity!!)
            .setView(dialogView)
            .setTitle("Symptoms for ${plant.plantName} (Sample ${plant.sampCorrelative})")
            .setIcon(R.drawable.ic_leaf_teal)

        //ghViewModel.getPlantSlots(plant.plantId, plant.requestId, plant.essayId, plant.essayId)

        val dialog = builder.create()
        dialog.show()

        val btnCamera = dialogView.btnCamera
        val btnOk = dialogView.btnOk
        val btnCancel = dialogView.btnCancel
        rvSymptoms = dialogView.rvSymptomsCheck

        var plantSlotList = mutableListOf<PlantSlot>()

        //ghViewModel.getPlantSlotsByPlant(plant.plantId, plant.requestId, plant.essayId, plant.activityId)

        println(ghViewModel.ghSlots.value!!
            .filter { it.sampleId == plant.sampleId && it.plantId == plant.plantId && it.activityId == plant.activityId })

        if (ghViewModel.ghSlots.value!!
                .filter { it.sampleId == plant.sampleId && it.plantId == plant.plantId && it.activityId == plant.activityId }
                .isNullOrEmpty()){
            ghViewModel.ghSymptoms.value!!.forEach{    //ghViewModel.symptomList
                val plantSlot = PlantSlot(
                    plantId = plant.plantId,
                    sampleId = plant.sampleId,
                    plantPosition = 1,
                    result = "negative",
                    rduType = 4,                    //----------- IT'S TEMPORARY!!
                    requestId = plant.requestId,
                    essayId = plant.essayId,
                    cropId = plant.cropId,
                    workflowId = plant.workflowId,
                    activityId = plant.activityId,
                    numOrderId = plant.numOrderId,
                    symptomId = it.symptomId,
                    symptomName = it.longName!!,
                    agentId = plant.agentId             // TAKE CARE
                )
                plantSlotList.add(plantSlot)
            }
        }else{
            plantSlotList = ghViewModel.ghSlots.value!!
                .filter { it.sampleId == plant.sampleId &&
                        it.plantId == plant.plantId &&
                        it.activityId == plant.activityId }.toMutableList()
        }

        rvSymptoms!!.layoutManager = LinearLayoutManager(activity)
        rvSymptoms!!.setItemViewCacheSize(100)
        rvSymptoms!!.adapter = SymptomCheckAdapter(
            plantSlotList.sortedBy { it.symptomName },
            this
        )
        rvSymptoms!!.itemAnimator = DefaultItemAnimator()

        btnCamera.setOnClickListener {
            val packageManager = activity?.packageManager
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(packageManager!!)?.also {
                    val photoFile: File? = try {
                        createImageFile(plant)
                    } catch (ex: IOException) {
                        println("Error creating Image File")
                        null
                    }
                    println("Photo file: $photoFile")
                    photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            this.context!!,
                            "com.example.xherrera.pathobindingtest.fileprovider",
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                    }
                }
            }.also {
                galleryAddPic()
            }
            startActivity(intent)
        }

        btnOk.setOnClickListener {
            println("Original plant slots: ${plantSlotList.count()}")
            val extraPlantSlots = mutableListOf<PlantSlot>()
            (activityList.filter { it.activityId > plant.activityId }).forEach { act ->
                plantSlotList.forEach { plantSlot ->
                    val extraPlantSlot = PlantSlot(
                        plantId = plantSlot.plantId,
                        sampleId = plantSlot.sampleId,
                        plantPosition = 1,
                        result = plantSlot.result,
                        rduType = 4,
                        requestId = plantSlot.requestId,
                        essayId = plantSlot.essayId,
                        cropId = plantSlot.cropId,
                        workflowId = plantSlot.workflowId,
                        activityId = act.activityId,
                        numOrderId = plantSlot.numOrderId,
                        symptomId = plantSlot.symptomId,
                        symptomName = plantSlot.symptomName,
                        agentId = plantSlot.agentId
                    )
                    extraPlantSlots.add(extraPlantSlot)
                }
                println("Extra plant slots: ${extraPlantSlots.count()}")
            }
            val totalPlantSlots = plantSlotList + extraPlantSlots
            println("Total plant slots: ${totalPlantSlots.count()}")
            ghViewModel.insertPlantSlots(plant.requestId, plant.essayId, totalPlantSlots)
            ghViewModel.getBioPack(plant.requestId, plant.essayId, plant.numOrderId)
            recyclerView?.adapter?.notifyItemChanged(position)
            recyclerView?.adapter?.notifyDataSetChanged()
            dialog.dismiss() //.hide()
        }

        btnCancel.setOnClickListener {
            ghViewModel.getBioPack(plant.requestId, plant.essayId, plant.numOrderId)
            dialog.dismiss()
        }

        /*
        val builder = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
        builder.setTitle("Symptoms for ${plant.plantName} (Sample ${plant.sampleId})")
        builder.setIcon(R.drawable.ic_leaf_teal)
        builder.setMultiChoiceItems(symptomArray, null) { _, which, isChecked ->
            if (isChecked){
                val auxSymptom = symptoms[which]
                selectedSymptoms.add(auxSymptom)
            } else if (selectedSymptoms.contains(symptoms[which])){
                selectedSymptoms.removeAt(Integer.valueOf(which))
            }
        }
        builder.setPositiveButton("OK"){_,_ ->
            println(selectedSymptoms)
            println(plant.plantName)
            recyclerView!!.adapter!!.notifyDataSetChanged()
        }
        val dialog = builder.create()
        dialog.show()
        */

        /*
        val navController = findNavController()

        navController.navigate(BioEssayDashFragmentDirections.actionBioEssayDashFragmentToPlantSympFragment(
            plant.requestId,
            plant.essayId,
            plant.plantId,
            plant.activityId,
            plant.sampleId,
            plant.workflowId,
            plant.numOrderId,
            plant.cropId,
            plant.agentId
        ))
        */
    }

    private fun createImageFile(plant: PlantBySample): File {
        println("createImageFile called!")
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        println("Time stamp: $timeStamp")
        val storageDir: File = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES) //getExternalStorageDirectory()
        println("Storage directory: $storageDir")

        val tempFile = File.createTempFile(
            "s${plant.sampCorrelative}_p${plant.plantId}_act${plant.activityId}_rq${plant.requestId}_${timeStamp}_",
            ".jpg",
            storageDir)
        println("Temp File path: ${tempFile.absolutePath}")

        return tempFile.apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
            println("Current Photo Path: $currentPhotoPath")
        }
    }

    private fun galleryAddPic() {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(currentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            context?.sendBroadcast(mediaScanIntent)
        }
    }

    override fun onItemClick(plantSlot: PlantSlot, position: Int) {

        println("Plant Symptom clicked: ${plantSlot.symptomName}")
        println("Position clicked: $position")
        when (plantSlot.result) {
            "negative" -> {
                plantSlot.result = "positive"
            }
            "positive" -> {
                plantSlot.result = "doubt"
            }
            "doubt" -> {
                plantSlot.result = "negative"
            }
        }
        rvSymptoms!!.adapter!!.notifyDataSetChanged()
    }
}

