package com.example.xherrera.pathobindingtest.lab

data class PcrGelBrief(
    val requestId: Int,
    val essayId: Int,
    val numOrderId: Int,
    val cropId: Int,
    val workflowId: Int,
    val activityId: Int,
    val supportId: Int,
    val controlQty: Int,
    val ladderQty: Int,
    val sampleQty: Int
)