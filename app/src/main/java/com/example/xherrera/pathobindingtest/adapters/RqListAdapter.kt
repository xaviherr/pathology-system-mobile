package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Request
import kotlinx.android.synthetic.main.rqlist_item.view.*

class RqListAdapter(
    private val requestList: List<Request>,
    private val listener: Listener
):RecyclerView.Adapter<RqListAdapter.ViewHolder>() {

    interface Listener {
        fun onItemClick(request: Request)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(requestList[position], listener, position)
    }

    override fun getItemCount(): Int = requestList.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rqlist_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        fun bind(
            request: Request,
            listener: Listener,
            pos: Int
        ){
            itemView.setOnClickListener { listener.onItemClick(request) }
            itemView.tvRqCode.text = request.code
            itemView.tvRqCropName.text = request.cropName
            itemView.tvRqDetails.text = request.details
            itemView.tvPathoQty.text = "Pathogens : "+request.pathogenQty
            itemView.tvSampleQty.text = "Samples : "+request.sampleQty
            itemView.tvReqDate.text = request.requiredDate
            itemView.tvEssays.text = "Essays : "+request.essayDetail
            itemView.tvSampleType.text = "Sample Type : "+request.sampleType
            /*
            if(pos%2 == 0){
                itemView.rvRequestItem.setBackgroundColor(Color.parseColor("#eeeeee"))
            }else{itemView.rvRequestItem.setBackgroundColor(Color.WHITE)}
            */
        }
    }
}