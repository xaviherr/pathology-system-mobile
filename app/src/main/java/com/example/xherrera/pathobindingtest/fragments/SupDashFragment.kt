package com.example.xherrera.pathobindingtest.fragments


import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.AccAdapter
import com.example.xherrera.pathobindingtest.adapters.SuppDashAdapter
import com.example.xherrera.pathobindingtest.database.entities.Activity
import com.example.xherrera.pathobindingtest.database.entities.ReadingDataUnit
import com.example.xherrera.pathobindingtest.database.entities.Slot
import com.example.xherrera.pathobindingtest.database.entities.Support
import com.example.xherrera.pathobindingtest.databinding.FragmentSupDashboardBinding
import com.example.xherrera.pathobindingtest.viewmodels.EssayViewModel
import com.example.xherrera.pathobindingtest.viewmodels.RequestViewModel
import com.google.android.material.button.MaterialButtonToggleGroup
import kotlinx.android.synthetic.main.fragment_sup_dashboard.*
import kotlinx.android.synthetic.main.fragment_sup_dashboard.view.*


class SupDashFragment : Fragment(), SuppDashAdapter.Listener, AccAdapter.Listener {

    lateinit var suppList: List<Support>
    lateinit var slotList: List<Slot>
    private val activityList: MutableList<Activity> = mutableListOf()
    private var requestId: Int ?= 0
    private var numOrderId: Int ?= 0
    private var essayId: Int ?= 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val args = SupDashFragmentArgs.fromBundle(arguments!!)
        requestId = args.rqId
        numOrderId = args.numOrderId
        essayId = args.essayId
        var activityId= 1
        var essayName = "DAS-ELISA"

        val binding = DataBindingUtil.inflate<FragmentSupDashboardBinding>(
            inflater,
            R.layout.fragment_sup_dashboard,
            container,
            false
        )

        if (args.essayId == 1) {
            activityId = 1
            essayName = "DAS-ELISA"
        }
        if (args.essayId == 5 || args.essayId == 12) {
            activityId = 24
            essayName = "NCM-ELISA"
        }
        if (args.essayId == 4) {
            activityId = 46
            essayName = "NASH"
        }

        binding.root.tvRequestInfo.text = "Request: ${args.rqCode}    Essay: $essayName"

        val toggleGroup = binding.btnGroupActivity

        if (args.essayId == 1) activityId = 1
        if (args.essayId == 5 || args.essayId == 12) activityId = 24
        if (args.essayId == 4) activityId = 46

        val essayViewModel= ViewModelProviders.of(activity!!).get(EssayViewModel::class.java)
        essayViewModel.getEssayPack(args.rqId, args.essayId, args.numOrderId)

        essayViewModel.essaySupports.observe(this, Observer {
            suppList = it
        })
        essayViewModel.essaySlots.observe(this, Observer {
            slotList = it
        })

        essayViewModel.essayActivities.observe(viewLifecycleOwner, Observer {
            it.forEach { activity -> activityList.add(activity); println("Activity: $activity") }
        })


        val recyclerView = binding.rvSupports
        val manager = LinearLayoutManager(activity)
        recyclerView.layoutManager = manager
        recyclerView.itemAnimator = DefaultItemAnimator()

        essayViewModel.essayPack.observe(this, Observer {
            recyclerView.adapter = SuppDashAdapter(it.essSupports.filter { support -> support.activityId == activityId },
                it.essSlots.filter { slot -> slot.activityId == activityId }, this)
        })

        val toggleListener = MaterialButtonToggleGroup.OnButtonCheckedListener { _, _, _ ->
            when {
                btnAct01.isChecked -> {
                    activityId = activityList[0].activityId
                    println("Activity selected $activityId")
                    essayViewModel.essayPack.observe(this, Observer {
                        recyclerView.adapter = SuppDashAdapter(it.essSupports.filter { support -> support.activityId == activityId },
                            it.essSlots.filter { slot -> slot.activityId == activityId }, this)
                    })
                    recyclerView.adapter!!.notifyDataSetChanged()
                }
                btnAct02.isChecked -> {
                    activityId = activityList[1].activityId
                    println("Activity selected $activityId")
                    essayViewModel.essayPack.observe(this, Observer {
                        recyclerView.adapter = SuppDashAdapter(it.essSupports.filter { support -> support.activityId == activityId },
                            it.essSlots.filter { slot -> slot.activityId == activityId }, this)
                    })
                    recyclerView.adapter!!.notifyDataSetChanged()
                }
                btnAct03.isChecked -> {
                    activityId = activityList[2].activityId
                    println("Activity selected $activityId")
                    essayViewModel.essayPack.observe(this, Observer {
                        recyclerView.adapter = SuppDashAdapter(it.essSupports.filter { support -> support.activityId == activityId },
                            it.essSlots.filter { slot -> slot.activityId == activityId }, this)
                    })
                    recyclerView.adapter!!.notifyDataSetChanged()
                }
                btnAct04.isChecked -> {
                    activityId = activityList[3].activityId
                    println("Activity selected $activityId")
                    essayViewModel.essayPack.observe(this, Observer {
                        recyclerView.adapter = SuppDashAdapter(it.essSupports.filter { support -> support.activityId == activityId },
                            it.essSlots.filter { slot -> slot.activityId == activityId }, this)
                    })
                    recyclerView.adapter!!.notifyDataSetChanged()
                }
                btnAct05.isChecked -> {
                    activityId = activityList[4].activityId
                    println("Activity selected $activityId")
                    essayViewModel.essayPack.observe(this, Observer {
                        recyclerView.adapter = SuppDashAdapter(it.essSupports.filter { support -> support.activityId == activityId },
                            it.essSlots.filter { slot -> slot.activityId == activityId }, this)
                    })
                    recyclerView.adapter!!.notifyDataSetChanged()
                }
            }
        }

        if (args.essayId == 1 || args.essayId == 5 || args.essayId == 12) {
            toggleGroup.addOnButtonCheckedListener(toggleListener)
            if (args.essayId == 5){
                binding.btnAct03.visibility = View.INVISIBLE
                binding.btnAct03.isClickable = false
                binding.btnAct03.isFocusable = false
                binding.btnAct03.isFocusableInTouchMode = false
                binding.btnAct04.visibility = View.INVISIBLE
                binding.btnAct04.isClickable = false
                binding.btnAct04.isFocusable = false
                binding.btnAct04.isFocusableInTouchMode = false
                binding.btnAct05.visibility = View.INVISIBLE
                binding.btnAct05.isClickable = false
                binding.btnAct05.isFocusable = false
                binding.btnAct05.isFocusableInTouchMode = false
            }
        } else {
            toggleGroup.visibility = View.INVISIBLE
        }


        binding.btnSendDistribution.setOnClickListener { v: View ->
            val rduList: ArrayList<ReadingDataUnit> = ArrayList()
            essayViewModel.essaySlots.observe(viewLifecycleOwner, Observer {
                val cropId = it.first().cropId
                val workflowId = it.first().workflowId
                val numOrderId = it.first().numOrderId
                for(i in it){
                    val rduType = when (i.type) {
                        "control" -> {11
                        }
                        "blind" -> {12
                        }
                        "empty" -> {73
                        }
                        "dBlind" -> {138
                        }
                        "10%" -> {139
                        }
                        "problem" -> {140
                        }
                        else -> 13
                    }
                    val rdu = ReadingDataUnit(
                        agentId = i.agentId,
                        sampleId = i.sampleId,
                        cellPosition = i.cellPosition,
                        result = i.result,
                        supportId = i.supportId,
                        activityId = i.activityId,
                        essayId = i.essayId,
                        requestId = requestId!!,
                        cropId = i.cropId,
                        numOrderId = i.numOrderId,
                        workFlowId = i.workflowId,
                        readingDataTypeId = rduType,
                        symptomId = 1
                    )
                    rduList.add(rdu)
                }
                println("RDUs: ${rduList.count()}")
                //Saving into the Room Database
                essayViewModel.insertRdus(requestId!!, essayId!!, rduList)
                //Posting through the API rest
                essayViewModel.workPostRduList(requestId!!, essayId!!, activityId, cropId, workflowId, numOrderId)
            })
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu_reset, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val args = SupDashFragmentArgs.fromBundle(arguments!!)

        if (item.itemId == R.id.resetRequest){
            AlertDialog
                .Builder(activity!!)
                .setTitle("Reset Request Warning")
                .setMessage("This action will delete your local data permanently.\nDo you want to proceed?")
                .setPositiveButton("RESET DATA"){_,_ ->
                    val requestViewModel = ViewModelProviders.of(this).get(RequestViewModel::class.java)
                    //DELETE DATA
                    requestViewModel.deleteEssayData(this.requestId!!, this.essayId!!)
                    requestViewModel.getRqDetails(requestId!!, numOrderId!!)
                    requestViewModel.getAgentsByProcess(requestId!!)
                    requestViewModel.getSamples(requestId!!, numOrderId!!)
                    requestViewModel.getActivities()
                    //NAVIGATE TO REQUEST LIST
                    findNavController().
                        navigate(SupDashFragmentDirections.
                            actionSupDashboardFragmentToRequestFragment())

                }
                .setNegativeButton("CANCEL", null)
                .create().show()
            return true
        }

        else {
            findNavController().navigate(SupDashFragmentDirections.actionSupDashboardFragmentToRequestFragment())
            return true
        }

        /*
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
                */
    }

    override fun onItemClick(support: Support) {
        AlertDialog.Builder(activity!!)
            .setTitle("Enter results")
            .setMessage(" Opening result recording view for ${support.supportId}")
            .setPositiveButton("Continue"){_,_ ->
                val args = SupDashFragmentArgs.fromBundle(arguments!!)
                findNavController().
                    navigate(SupDashFragmentDirections.
                        actionSupDashboardFragmentToLectureRegFragment
                            (support.availableSlots, args.rqId, support.supportId, args.essayId, support.activityId, args.numOrderId, support.agentName?:"Null", args.rqCode))
            }
            .setNegativeButton("Cancel", null)
            .create().show()
    }
}
