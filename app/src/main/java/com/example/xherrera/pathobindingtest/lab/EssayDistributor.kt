package com.example.xherrera.pathobindingtest.lab

data class EssayDistributor(
    val controlPositionList: ArrayList<Int>,
    val tenPercentPositionList: ArrayList<Int>,
    val problemPositionList: ArrayList<Int>,
    val blindPositionList: ArrayList<Int>,
    val dBlindPositionList: ArrayList<Int>,
    val emptyPositionList: ArrayList<Int>
)