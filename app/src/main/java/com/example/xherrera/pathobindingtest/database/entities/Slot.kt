package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["cellPosition", "requestId", "essayId", "supportId", "activityId", "agentId"])
data class Slot (
    @SerializedName("labProcessId") @Expose @ColumnInfo(name ="labProcessId") val labProcessId: Int,
    @SerializedName("agentId") @Expose @ColumnInfo(name ="agentId") val agentId: Int = 16,
    @SerializedName("agentName") @Expose @ColumnInfo(name ="agentName") val agentName: String = "Agent",
    @SerializedName("sampleId") @Expose @ColumnInfo(name ="sampleId") var sampleId: Int = 15,
    @SerializedName("supportId") @Expose @ColumnInfo(name ="supportId") var supportId: Int = 16,
    @SerializedName("requestId") @Expose @ColumnInfo(name ="requestId") var requestId: Int = 16,
    @SerializedName("essayId") @Expose @ColumnInfo(name ="essayId") var essayId: Int,
    @SerializedName("cellPosition") @Expose @ColumnInfo(name ="cellPosition") val cellPosition: Int,
    @SerializedName("result") @Expose @ColumnInfo(name ="result") var result: String ?= "negative",
    @SerializedName("type") @Expose @ColumnInfo(name ="type") var type: String = "empty",
    @SerializedName("cropId") @Expose @ColumnInfo(name = "cropId") val cropId: Int,
    @SerializedName("workFlowId") @Expose @ColumnInfo(name = "workFlowId") val workflowId: Int,
    @SerializedName("activityId") @Expose @ColumnInfo(name = "activityId") val activityId: Int,
    @SerializedName("numOrderId") @Expose @ColumnInfo(name = "numOrderId") val numOrderId: Int
)