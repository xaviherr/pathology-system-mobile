package com.example.xherrera.pathobindingtest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Symptom
import kotlinx.android.synthetic.main.symptom_item.view.*

class SymptomsAdapter(
    private val symptomList: List<Symptom>,
    private val listener: Listener
): RecyclerView.Adapter<SymptomsAdapter.ViewHolder>() {

    interface Listener{
        fun onItemClick(symptom: Symptom)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.symptom_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(symptomList[position], listener)
    }

    override fun getItemCount(): Int = symptomList.count()

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(
            symptom: Symptom,
            listener: Listener
        ){
            val posName: String = if (symptom.plantPositionId == 1) "API" else if (symptom.plantPositionId == 2) "MED" else "BAS"
            itemView.imgDelete.setOnClickListener { listener.onItemClick(symptom) }
            itemView.tvSympShortName.text = "${symptom.longName} ($posName)"
        }
    }
}