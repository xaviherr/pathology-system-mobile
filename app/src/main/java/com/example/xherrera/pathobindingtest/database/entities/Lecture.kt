package com.example.xherrera.pathobindingtest.database.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Lecture(
    @SerializedName("sampleId") @Expose val sampleId: Int,
    @SerializedName("supportId") @Expose val supportId: Int,
    @SerializedName("cellPosition") @Expose val cellPosition: Int,
    @SerializedName("result") @Expose val result: String
)