package com.example.xherrera.pathobindingtest.database.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Agent(
   @SerializedName("requestId") @Expose val agentId: Int,
   @SerializedName("agentName") @Expose val agentName: String
)