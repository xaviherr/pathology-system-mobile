package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Slot
import kotlinx.android.synthetic.main.dash_child_item.view.*

class ChildDashAdapter(
    private val slotList: ArrayList<Slot>
): RecyclerView.Adapter<ChildDashAdapter.ViewHolder>() {

    private val colors: Array<String> = arrayOf(
        "#D1C4E9",
        "#C8E6C9",
        "#80D8FF",
        "#FFCDD2",
        "#CCFF90",
        "#B2DFDB",
        "#A7FFEB",
        "#C5CAE9",
        "#90CAF9",
        "#E1BEE7",
        "#80DEEA",
        "#69F0AE",
        "#EA80FC"
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dash_child_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(slotList[position], colors)
    }

    override fun getItemCount(): Int {
        return slotList.count()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(
            slot: Slot,
            colors:Array<String>
        ){
            itemView.tvDashCell.text = slot.labProcessId.toString()
            itemView.tvDashPos.text = slot.cellPosition.toString()
            when {
                slot.type == "empty" -> {
                    itemView.cvDashChild.setCardBackgroundColor(Color.WHITE)
                    itemView.tvDashCell.text = "E"
                }
                slot.type == "blind" -> {
                    itemView.cvDashChild.setCardBackgroundColor(Color.parseColor("#9E9E9E"))
                    itemView.tvDashCell.text = "B"
                    itemView.tvDashCell.setTextColor(Color.WHITE)
                    itemView.tvDashPos.setTextColor(Color.WHITE)
                }
                slot.type == "dBlind" -> {
                    itemView.cvDashChild.setCardBackgroundColor(Color.parseColor("#9E9E9E"))
                    itemView.tvDashCell.text = "DB"
                    itemView.tvDashCell.setTextColor(Color.WHITE)
                    itemView.tvDashPos.setTextColor(Color.WHITE)
                }
                slot.type == "problem" -> {
                    itemView.cvDashChild.setCardBackgroundColor(Color.parseColor("#AD1457"))
                    itemView.tvDashCell.text = "P"
                    itemView.tvDashCell.setTextColor(Color.WHITE)
                    itemView.tvDashPos.setTextColor(Color.WHITE)
                }
                slot.type == "control" -> {
                    itemView.cvDashChild.setCardBackgroundColor(Color.parseColor("#FFF59D"))
                    itemView.tvDashCell.text = "C"
                }
                slot.type == "10%" -> {
                    itemView.cvDashChild.setCardBackgroundColor(Color.parseColor("#FF8A65"))
                    itemView.tvDashCell.setTextColor(Color.WHITE)
                    itemView.tvDashPos.setTextColor(Color.WHITE)
                }
                else -> {
                    itemView.cvDashChild.setCardBackgroundColor(Color.parseColor(colors[slot.agentId%13]))
                }
            }
        }
    }

}