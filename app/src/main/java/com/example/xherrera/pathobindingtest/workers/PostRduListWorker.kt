package com.example.xherrera.pathobindingtest.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.xherrera.pathobindingtest.database.PathoDatabase
import com.example.xherrera.pathobindingtest.extDb.ApiService

class PostRduListWorker(context: Context, params: WorkerParameters): Worker(context, params) {

    private val dBase by lazy {
        PathoDatabase.getInstance(context)
    }
    private val apiServe by lazy {
        ApiService.create()
    }

    override fun doWork(): Result {
        makeStatusNotification("The results were sent", applicationContext)

        val requestId = inputData.getInt("rqId", 1)
        val essayId = inputData.getInt("essId", 1)
        val activityId = inputData.getInt("actId", 1)
        val numOrderId = inputData.getInt("numOrderId", 1)

        val localRduList = dBase.readingDataUnitDao().getRequestSpecRdus(requestId, essayId, activityId, numOrderId)
        println("Local RDU to upload: ${localRduList.count()}")

        val response = apiServe.postRduArray(localRduList).execute()

        if (response.isSuccessful){
            println("RDU to upload: ${localRduList.count()}")
            return Result.success()
        } else{
            if (response.code() in 500..999){
                return Result.retry()
            }
            return Result.failure()
        }
    }
}