package com.example.xherrera.pathobindingtest.fragments


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.databinding.FragmentHomeBinding
import com.example.xherrera.pathobindingtest.fragments.HomeFragmentDirections
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: FragmentHomeBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_home, container, false)

        binding.btnGoToRequests.setOnClickListener { v: View ->
            v.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToRequestFragment())
        }

        binding.homeConstraint.btnGoToReports.setOnClickListener { v: View ->
            v.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToReportsFragment())
        }

        binding.homeConstraint.btnGoToReagentsMan.setOnClickListener { v: View ->
            v.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToReagentsManFragment()) }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}
