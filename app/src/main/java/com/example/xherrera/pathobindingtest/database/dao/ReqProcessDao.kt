package com.example.xherrera.pathobindingtest.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.ReqProcess

@Dao
interface ReqProcessDao {

    @Query("SELECT * FROM ReqProcess")
    fun getReqProcesses():LiveData<List<ReqProcess>>

    @Query("SELECT * FROM ReqProcess")
    fun getReqProcessList(): List<ReqProcess>

    @Query("SELECT * FROM ReqProcess WHERE requestId = :requestId")
    suspend fun getReqProcWhere(requestId: Int): List<ReqProcess>

    @Query("SELECT * FROM ReqProcess WHERE requestId = :requestId AND essayId = :essayId")
    suspend fun getSpecificReqProc(requestId: Int, essayId: Int): ReqProcess

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(reqProcess: ReqProcess)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(reqProcess: ReqProcess)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(reqProcessList: List<ReqProcess>)

    @Query("DELETE FROM ReqProcess WHERE requestId = :requestId")
    fun deleteProcesses(requestId: Int)

}